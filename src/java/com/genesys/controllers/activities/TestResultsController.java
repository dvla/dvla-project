/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genesys.controllers.activities;

import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.entity.driver_services.DriverServices;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.entity.system.ApplicantServiceActivity;
import com.ernest.genesys.entity.system.ServiceActivity;
import com.ernest.genesys.services.invoicing.DriverInvoiceServices;
import com.ernest.genesys.services.util.CrudService;
import com.stately.modules.web.jsf.Msg;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.inject.Inject;

/**
 *
 * @author eugene
 */
@Named(value = "testResultsController")
@SessionScoped
public class TestResultsController implements Serializable {

    @Inject
    private CrudService crude;
    @Inject
    private DriverInvoiceServices service;
    private String invoiceNumber;

    private ApplicantServiceActivity applicantActivity;

    private List<ServiceActivity> activities = new ArrayList<>();

    private Transactions transactions;
    private boolean exist = false;

    public TestResultsController() {
    }

    public void saveResultsDetails() {
        try {
            if (invoiceNumber == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please Enter Invoice Number....."));
                return;
            }

            if (transactions == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please check your invoice details:..."));
                return;
            }

            for (ServiceActivity sa : activities) {
                applicantActivity = new ApplicantServiceActivity();
                applicantActivity.setActivityStatus(sa.getStatus());
                applicantActivity.setApplicant(transactions.getApplicantId());
                applicantActivity.setServiceActivity(sa);
                crude.save(applicantActivity);
            }

            Msg.successSave();
        } catch (Exception e) {
            System.out.println("Error saving details of result : " + e.getCause().getLocalizedMessage());
            Msg.failedSave();
        }
    }

    public void checkForInvoice() {
        try {
            System.out.println("Invoice Number entered is : " + invoiceNumber);
            transactions = service.getTransactionsByInvoiceNumber(invoiceNumber);
            if (transactions == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Invoice Number does not exist, please check again"));
                return;
            }

            Applicant applicant = transactions.getApplicantId();
            String reason = transactions.getReason();

            System.out.println("Driver Service in request is :  " + transactions.getServiceCode().getServiceName());
            List<ApplicantServiceActivity> activitys = service.getApplicantActivities(applicant);
            if (!activitys.isEmpty()) {
                activities = new ArrayList<>();
                for (ApplicantServiceActivity apa : activitys) {
                    ServiceActivity sa = new ServiceActivity();
                    sa.setActivity(apa.getServiceActivity().getActivity());
                    sa.setStatus(apa.getActivityStatus());
                    sa.setService(apa.getServiceActivity().getService());
                    activities.add(sa);
                }
            } else {
                DriverServices driverService = transactions.getServiceCode();
                activities = new ArrayList<>();
                if (reason == null) {
                    activities = service.getAllServiceActivitiesByDriverService(driverService);
                    if (activities.isEmpty()) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("No Activities configured for this driver service request, please configure before proceeding"));
                        return;
                    }
                } else {
                    //we check for the type of reason here and render the appropriate page content as such
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Success!!"));
                exist = true;
            }
        } catch (Exception e) {
            System.out.println("Error finding by invoice number: " + e.getLocalizedMessage());
        }
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public ApplicantServiceActivity getApplicantActivity() {
        return applicantActivity;
    }

    public void setApplicantActivity(ApplicantServiceActivity applicantActivity) {
        this.applicantActivity = applicantActivity;
    }

    public List<ServiceActivity> getActivities() {
        return activities;
    }

    public void setActivities(List<ServiceActivity> activities) {
        this.activities = activities;
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }

}
