/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genesys.eugene.model;

import javax.sql.DataSource;
import java.sql.Connection;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
/**
 *
 * @author eugene
 */
public class DBConnect {
    private static Logger logger = Logger.getLogger(DBConnect.class.getName());
    public static Connection getConnection() throws Exception {
        Context ctx = null;
        try
        {
            logger.debug("Getting connection.");
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/dvla");
            
            return ds.getConnection();
        } catch (Exception e) {
            logger.error("Problem getting connection: " + e.getMessage());
            throw new Exception(e.getMessage(), e.getCause());
        } finally {
            if (ctx != null) {
                ctx.close();
            }
        }
    }
}
