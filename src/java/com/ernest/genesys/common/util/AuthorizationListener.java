package com.ernest.genesys.common.util;

import com.stately.modules.web.jsf.JsfUtil;
import javax.faces.application.NavigationHandler;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Daud
 */
public class AuthorizationListener implements PhaseListener {

    private static final Logger log = Logger.getLogger(AuthorizationListener.class.getName());

    @Override
    public void afterPhase(PhaseEvent event) {
        String currentPage = JsfUtil.getFacesContext().getViewRoot().getViewId();
        log.info("THE CURRENT PAGE IS {0}" + currentPage);

        if (JsfUtil.getHTTPSession() == null) {
            NavigationHandler nh = JsfUtil.getFacesContext().getApplication().getNavigationHandler();
            nh.handleNavigation(JsfUtil.getFacesContext(), null, "sessionExpired");
        }
    }

    @Override
    public void beforePhase(PhaseEvent event) {
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;

    }
}
