package com.ernest.genesys.common.util;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company SiccusNet INC
 * @email ainoodauda@gmail.com / daud@siccusNet.com
 * @date 06 June 2016
 * @time Jun 6, 2016 1:18:22 AM
 */
public class PageCommonInputs {

    private String searchParameter = null;
    private String searchValue = null;
    private boolean showComponent = true;

    public String getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(String searchParameter) {
        this.searchParameter = searchParameter;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public boolean isShowComponent() {
        return showComponent;
    }

    public void setShowComponent(boolean showComponent) {
        this.showComponent = showComponent;
    }

}
