/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.common.util;

import com.stately.modules.jpa2.UniqueEntityModel2;

/**
 *
 * @author IconU1
 */
public interface GenesysUniqueEntityModelMethod {
   public void saveMethod();
   public void clearMethod();
   //public void selectMethod(EntityModel2 entityModel2);
   public void selectMethod(UniqueEntityModel2 uniqueEntityModel2);
  // public void deleteMethod(EntityModel2 entityModel2);
   public void deleteMethod(UniqueEntityModel2 uniqueEntityModel2);
}
