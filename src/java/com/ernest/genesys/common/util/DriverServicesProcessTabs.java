package com.ernest.genesys.common.util;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 03 June 2016
 * @time Jun 3, 2016 6:02:39 AM
 */
public class DriverServicesProcessTabs {

    private String appliedService = "";
    private String serviceTestResults = "";


    public String getServiceTestResults() {
        return serviceTestResults;
    }

    public String getAppliedService() {
        return appliedService;
    }

    public void setAppliedService(String appliedService) {
        this.appliedService = appliedService;
    }

    public void setServiceTestResults(String serviceTestResults) {
        this.serviceTestResults = serviceTestResults;
    }

}
