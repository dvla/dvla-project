package com.ernest.genesys.common.util;

import com.ernest.genesys.services.util.CrudService;
import com.google.common.base.Strings;
import com.stately.common.utils.GenUUID;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 */
@Named(value = "appCommonOptions")
@SessionScoped
public class AppCommonUtils implements Serializable {

    @Inject
    CrudService crudService;

    public static void main(String[] args) {

    }

    public AppCommonUtils() {
    }

    public static DriverServicesProcessTabs getCurrentProcessStep(String currentProcess) {

        DriverServicesProcessTabs processTabs = new DriverServicesProcessTabs();

        if (currentProcess.equalsIgnoreCase("1")) {
            processTabs.setAppliedService("active");
        } else if (currentProcess.equalsIgnoreCase("2")) {
            processTabs.setServiceTestResults("active");
        }
        return processTabs;
    }

    public static String setEntityId(String id) {

        if (Strings.isNullOrEmpty(id)) {
            return GenUUID.getRandomUUID();
        }
        return id;
    }

    public SelectItem[] getCountryList() {
        SelectItem[] countrySelectItems = null;
        try {
            List<String> listOfCountries = new ArrayList<>();
            listOfCountries.addAll(Arrays.asList(Locale.getISOCountries()));
            countrySelectItems = new SelectItem[listOfCountries.size() + 1];
            countrySelectItems[0] = new SelectItem("", "Select One");
            int counter = 1;
            for (String eachCountry : listOfCountries) {
                Locale obj = new Locale("", eachCountry);
                countrySelectItems[counter++] = new SelectItem(obj.getDisplayCountry(), obj.getDisplayCountry());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return countrySelectItems;
    }

    public SelectItem[] getPassOrFailedList(String headerTitle) {
        SelectItem[] dataSelectItems = null;
        try {
            dataSelectItems = new SelectItem[3];
            dataSelectItems[0] = new SelectItem(null, headerTitle);
            dataSelectItems[1] = new SelectItem(true, "Passed");
            dataSelectItems[2] = new SelectItem(false, "Failed");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataSelectItems;
    }

}
