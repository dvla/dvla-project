/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.enums;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author Ernest
 */
public enum ServiceCategory implements MessageResolvable{
    VEHICLE("Vehicle"),
    DRIVER("Driver");
    
    
    String desc;

    private ServiceCategory(String desc) {
        this.desc = desc;
    }
    
    

    @Override
    public String getCode() {
        return desc;
    }

    @Override
    public String getLabel() {
       return desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc ;
    }
    
    
    
}
