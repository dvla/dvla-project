package com.ernest.genesys.enums;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 08 June 2016
 */
public enum TransactionStatus {
    ARCHIVED

}
