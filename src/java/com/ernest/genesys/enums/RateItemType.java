package com.ernest.genesys.enums;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author Ernest
 */
public enum RateItemType implements MessageResolvable{
    VEHICLE("Vehicle"),
    DRIVER("Driver");
    
    
    String desc;

    private RateItemType(String desc) {
        this.desc = desc;
    }
    
    

    @Override
    public String getCode() {
        return desc;
    }

    @Override
    public String getLabel() {
       return desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc ;
    }
    
    
    
}
