/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.enums;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author eugene
 */
public enum ServiceTestType implements MessageResolvable {

    IN_TRAFFIC_TEST("in_traffic_test", "In-Traffic Test"),
    EYE_TEST("eye_test", "Eye Test"),
    COFC_AUTHENTICATION("cofc_authentication", "COFC Authentication"),
    BIOMETRIC_CAPTURE("biometric_capture", "Biometric Capture"),
    PAYMENT("payment", "Payment");

    private String code;
    private String label;

    private ServiceTestType(String code, String label) {
        this.code = code;
        this.label = label;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
