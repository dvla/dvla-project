package com.ernest.genesys.enums;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 05 June 2016
 */
public enum ServiceStatus {
    PENDING, PROCESSING,COMPLETED, PRINTED, APPROVED, DECLINED,FAILED
}
