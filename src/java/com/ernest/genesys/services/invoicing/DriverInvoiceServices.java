package com.ernest.genesys.services.invoicing;

import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.entity.driver_services.DriverServices;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.entity.system.ApplicantServiceActivity;
import com.ernest.genesys.enums.ServiceStatus;
import com.ernest.genesys.entity.system.ServiceActivity;
import com.ernest.genesys.services.util.CrudService;
import com.stately.modules.jpa2.QryBuilder;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daud
 */
@Stateless
public class DriverInvoiceServices implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private static final Logger LOGGER = Logger.getLogger(CrudService.class.getName());

    public Transactions searchInvoiceNumber(String invoiceNo, ServiceStatus status) {
        try {
            QryBuilder builder = new QryBuilder(em, Transactions.class);
            builder.addStringQryParam(Transactions._invoiceNo, invoiceNo, QryBuilder.ComparismCriteria.EQUAL);
            builder.addObjectParam(Transactions._status, status);
            return (Transactions) builder.buildQry().getSingleResult();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public List<ServiceActivity> getServiceTests(Transactions transactions) {
        try {

            QryBuilder builder = new QryBuilder(em, ServiceActivity.class);
            builder.addObjectParam(ServiceActivity._service, transactions.getServiceCode());
            return builder.buildQry().getResultList();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<ApplicantServiceActivity> searchApplicantServiceActivities(Transactions transactions) {
        try {

            QryBuilder builder = new QryBuilder(em, ApplicantServiceActivity.class);
            builder.addObjectParam(ApplicantServiceActivity._transactions, transactions);
            builder.addObjectParam(ApplicantServiceActivity._applicant, transactions.getApplicantId());
            return builder.buildQry().getResultList();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public List<ApplicantServiceActivity> searchApplicantServiceActivities(Transactions transactions, Applicant applicant) {
        try {

            QryBuilder builder = new QryBuilder(em, ApplicantServiceActivity.class);
            builder.addObjectParam(ApplicantServiceActivity._transactions, transactions);
            builder.addObjectParam(ApplicantServiceActivity._applicant, applicant);
            System.out.println("the ........ "+builder.getQryInfo());
            return builder.buildQry().getResultList();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public Transactions getTransactionsByInvoiceNumber(String invoice) {
        try {
            return (Transactions) em.createNamedQuery(Transactions.FIND_TRANACTIONS_BY_INVOICE_NUMBER).setParameter("invoiceno", invoice).getSingleResult();
        } catch (Exception e) {
            System.out.println("Error loading Transactions by Invoice Number : " + e.getCause().getLocalizedMessage());
        }
        return null;
    }

    public List<ApplicantServiceActivity> getApplicantActivities(Applicant applicant) {
        try {
            return (List<ApplicantServiceActivity>) em.createNamedQuery(ApplicantServiceActivity.FIND_SERVICE_ACTIVITY_BY_APPLICANT).setParameter("applicant", applicant).getResultList();
        } catch (Exception e) {
            System.out.println("Error in getting activities done by applicant, applicant is doing this the first time : " + e.getLocalizedMessage());
        }
        return Collections.EMPTY_LIST;
    }

    public List<ServiceActivity> getAllServiceActivitiesByDriverService(DriverServices service) {
        try {
            return (List<ServiceActivity>) em.createNamedQuery(ServiceActivity.FIND_DRIVER_SERVICE_ACTIVITIES).setParameter("service", service).getResultList();
        } catch (Exception e) {
            System.out.println("Error getting driver's request service activities : " + e.getCause().getLocalizedMessage());
        }
        return Collections.EMPTY_LIST;
    }

}
