package com.ernest.genesys.services.common;

import com.ernest.genesys.entity.driver_services.DriverServices;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.entity.system.DvlaCenters;
import com.ernest.genesys.entity.system.ServiceActivity;
import com.ernest.genesys.entity.system.UserAccessAccount;
import com.ernest.genesys.entity.system_old.AccountCategory;
import com.ernest.genesys.entity.system_old.DvlaAccount;
import com.ernest.genesys.entity.system_old.InsuranceCompany;
import com.ernest.genesys.entity.system_old.RateItems;
import com.ernest.genesys.entity.system_old.VehicleMake;
import com.ernest.genesys.services.util.CrudService;
import com.icsecurities.common.entities.Country;
import com.stately.modules.jpa2.CrudController;
import com.stately.modules.jpa2.Enviroment;
import com.stately.modules.jpa2.QryBuilder;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author IconU1
 */
@Stateless
public class CommonServices extends CrudController implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private static final Logger LOGGER = Logger.getLogger(CrudService.class.getName());

    @PostConstruct
    private void init() {
        setEm(em);
        setEnviroment(Enviroment.JAVA_EE);

        System.out.println("new crud service created ... " + toString());
    }

    @SuppressWarnings("unchecked")
    public List findByColumnName(Class clazz, String field, String value) {

        try {
            String qry = "SELECT e FROM " + clazz.getSimpleName() + " e "
                    + "WHERE e." + field + " LIKE '%" + value + "%'";

            return em.createQuery(qry).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public UserAccessAccount validateUser(String username, String password) {
        try {

            QryBuilder builder = new QryBuilder(em, UserAccessAccount.class);
            builder.addStringQryParam(UserAccessAccount._username, username, QryBuilder.ComparismCriteria.EQUAL);
            builder.addStringQryParam(UserAccessAccount._password, password, QryBuilder.ComparismCriteria.EQUAL);

            return (UserAccessAccount) builder.buildQry().getSingleResult();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public List searchByColumnName(Class clazz, String searchValue, String searchAttribute) {
        QryBuilder builder = new QryBuilder(em, clazz);
        builder.addStringQryParam(searchAttribute, searchValue, QryBuilder.ComparismCriteria.LIKE);
        return builder.buildQry().getResultList();

    }

    public List findByFieldName(Class clazz, String searchAttribute, String searchValue) {
        QryBuilder builder = new QryBuilder(em, clazz);
        builder.addStringQryParam(searchAttribute, searchValue, QryBuilder.ComparismCriteria.LIKE);
        return builder.buildQry().getResultList();
    }

    public List findStrictByFieldName(Class clazz, String searchAttribute, String searchValue) {
        QryBuilder builder = new QryBuilder(em, clazz);
        builder.addStringQryParam(searchAttribute, searchValue, QryBuilder.ComparismCriteria.EQUAL);
        return builder.buildQry().getResultList();
    }

    public List findAll(Class clazz, boolean searchAll) {

        QryBuilder builder = new QryBuilder(em, clazz);
        if (!searchAll) {
            builder.addObjectParam("deleted", searchAll);
        }

        return builder.buildQry().getResultList();
    }

    public List<DvlaCenters> getAllDvlaCenter() {
        QryBuilder builder = new QryBuilder(em, DvlaCenters.class);
        builder.orderByAsc(DvlaCenters._centerName);

        return builder.buildQry().getResultList();

    }

    public List<Country> countrysList() {
        QryBuilder builder = new QryBuilder(em, Country.class);
        return builder.buildQry().getResultList();
    }

    public List<DriverServices> getAllDriverServices() {
        QryBuilder builder = new QryBuilder(em, DriverServices.class);
        builder.orderByAsc(DriverServices._serviceName);
        return builder.buildQry().getResultList();
    }

    public List<VehicleMake> getAllVehicleMake() {
        QryBuilder builder = new QryBuilder(em, VehicleMake.class);
        //builder.addObjectParam("deleted", false);
        return builder.buildQry().getResultList();
    }

    public List<AccountCategory> getAllAccountCategory() {
        QryBuilder builder = new QryBuilder(em, AccountCategory.class);
        //builder.addObjectParam("deleted", false);
        return builder.buildQry().getResultList();
    }

    public List<DvlaAccount> getAllDvlaAccount() {
        QryBuilder builder = new QryBuilder(em, DvlaAccount.class);
        //builder.addObjectParam("deleted", false);
        return builder.buildQry().getResultList();
    }

    public List<RateItems> getAllRateItems() {
        QryBuilder builder = new QryBuilder(em, RateItems.class);
        //builder.addObjectParam("deleted", false);
        return builder.buildQry().getResultList();
    }

    public List<Country> getCountries() {
        QryBuilder builder = new QryBuilder(em, Country.class);
        return builder.buildQry().getResultList();
    }

    public List<InsuranceCompany> searchInsuranceCompany(String searchValue, String searchAttribute) {
        QryBuilder builder = new QryBuilder(em, InsuranceCompany.class);
        builder.addStringQryParam(searchAttribute, searchValue, QryBuilder.ComparismCriteria.LIKE);
        return builder.buildQry().getResultList();
    }

}
