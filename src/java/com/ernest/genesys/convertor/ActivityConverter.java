package com.ernest.genesys.convertor;

import com.ernest.genesys.entity.system.Activity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import org.omnifaces.converter.SelectItemsConverter;

@FacesConverter(forClass = Activity.class)
public class ActivityConverter extends SelectItemsConverter {

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        Activity tId = (Activity) value;
        if (tId != null) {
            if (component.getId().toLowerCase().contains("name")) {
                return tId.toString();
            }

            return tId.getId();
        }
        return null;
    }
}
