package com.ernest.genesys.convertor;


import com.ernest.genesys.entity.driver_services.Transactions;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import org.omnifaces.converter.SelectItemsConverter;

@FacesConverter(forClass=Transactions.class)
public class TransactionsConverter extends SelectItemsConverter
{
 
  @Override
  public String getAsString(FacesContext facesContext, UIComponent component, Object value)
  {
      Transactions tId   = (Transactions) value;
    if (tId != null)
    {
      if (component.getId().toLowerCase().contains("name"))
      {
        return tId.toString();
      }

      return tId.getTransactionId();
    }
    return null;
  }
}