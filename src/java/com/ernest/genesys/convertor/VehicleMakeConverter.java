package com.ernest.genesys.convertor;







import com.ernest.genesys.entity.system_old.VehicleMake;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import org.omnifaces.converter.SelectItemsConverter;

@FacesConverter(forClass=VehicleMake.class)
public class VehicleMakeConverter extends SelectItemsConverter
{
 
  @Override
  public String getAsString(FacesContext facesContext, UIComponent component, Object value)
  {
      VehicleMake vehicleMake   = (VehicleMake) value;
    if (vehicleMake != null)
    {
      if (component.getId().toLowerCase().contains("name"))
      {
        return vehicleMake.toString();
      }

      return vehicleMake.getId();
    }
    return null;
  }
}