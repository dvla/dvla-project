package com.ernest.genesys.convertor;


import com.ernest.genesys.entity.system_old.DvlaAccount;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import org.omnifaces.converter.SelectItemsConverter;

@FacesConverter(forClass=DvlaAccount.class)
public class DvlaAccountConverter extends SelectItemsConverter
{
 
  @Override
  public String getAsString(FacesContext facesContext, UIComponent component, Object value)
  {
      DvlaAccount vehicleMake   = (DvlaAccount) value;
    if (vehicleMake != null)
    {
      if (component.getId().toLowerCase().contains("name"))
      {
        return vehicleMake.toString();
      }

      return vehicleMake.getId();
    }
    return null;
  }
}