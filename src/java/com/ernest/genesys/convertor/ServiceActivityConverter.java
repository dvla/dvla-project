package com.ernest.genesys.convertor;

import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.entity.system.ServiceActivity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import org.omnifaces.converter.SelectItemsConverter;

@FacesConverter(forClass = ServiceActivity.class)
public class ServiceActivityConverter extends SelectItemsConverter {

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        ServiceActivity tId = (ServiceActivity) value;
        if (tId != null) {
            if (component.getId().toLowerCase().contains("name")) {
                return tId.toString();
            }

            return tId.getId();
        }
        return null;
    }
}
