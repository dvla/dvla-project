package com.ernest.genesys.convertor;

import com.ernest.genesys.entity.system.ApplicantServiceActivity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import org.omnifaces.converter.SelectItemsConverter;

@FacesConverter(forClass = ApplicantServiceActivity.class)
public class ApplicantServiceConverter extends SelectItemsConverter {

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        ApplicantServiceActivity tId = (ApplicantServiceActivity) value;
        if (tId != null) {
            if (component.getId().toLowerCase().contains("name")) {
                return tId.toString();
            }

            return tId.getId();
        }
        return null;
    }
}
