/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers;

import com.ernest.genesys.entity.system.UserAccessAccount;
import com.ernest.genesys.services.common.CommonServices;
import com.stately.modules.web.jsf.JsfUtil;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.omnifaces.util.Faces;

/**
 *
 * @author IconU1
 */
@Named(value = "loginController")
@RequestScoped
public class LoginController {

    /**
     * Creates a new instance of LoginController
     */
    private String username;
    private String password;

    @Inject
    private UserSession userSession;
    @Inject
    CommonServices commonServices;

    public LoginController() {
    }

    public void loginButtonAction() {

        try {
            UserAccessAccount userAccessAccount = commonServices.validateUser(username, password);
            if (null != userAccessAccount) {

                userSession.setUserAccessAccount(userAccessAccount);
                userSession.setHasUserLogin(true);
                Faces.redirect("app/secured/dashboard.xhtml?faces-redirect=false");
            } else {
                Faces.redirect("app/login.xhtml?faces-redirect=false");
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void logoutUser() {

        try {
            userSession.setHasUserLogin(false);
            JsfUtil.invalidateSession();
            JsfUtil.resetViewRoot();
            Faces.redirect("app/login.xhtml?faces-redirect=false");
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String registrater() {
        return "/secured/registration/masters/registration_form.xhtml";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserSession getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
