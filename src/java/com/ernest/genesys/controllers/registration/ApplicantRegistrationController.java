/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.registration;

import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "applicantRegistration")
@SessionScoped
public class ApplicantRegistrationController implements Serializable {

    private Applicant applicantInformation = new Applicant();
    private List<Applicant> applicantInformationsList = new ArrayList<>();
    @Inject
    private CrudService crudService;

    public ApplicantRegistrationController() {
    }

    public void saveApplicantInformation() {
//        idGenerator.uniqueEntity2(applicantInformation);
        applicantInformation.setId(GenUUID.getRandomUUID());
        if (crudService.save(applicantInformation) != null) {
            Msg.successSave();
            applicantInformationsList = new ArrayList<>();
            applicantInformationsList.add(applicantInformation);

            applicantInformation = new Applicant();
        } else {
            Msg.failedSave();
        }
    }

    public void clear() {
        applicantInformation = new Applicant();
    }

    public void selectApplicant(Applicant information) {
        applicantInformation = information;
    }

    public void deleteApplicant(Applicant information) {
        if (crudService.delete(information, true)) {
            Msg.successDelete();
        } else {
            Msg.failedDelete();
        }
    }

    public Applicant getApplicantInformation() {
        return applicantInformation;
    }

    public void setApplicantInformation(Applicant applicantInformation) {
        this.applicantInformation = applicantInformation;
    }

    public List<Applicant> getApplicantInformationsList() {
        return applicantInformationsList;
    }

    public void setApplicantInformationsList(List<Applicant> applicantInformationsList) {
        this.applicantInformationsList = applicantInformationsList;
    }

}
