/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.registration;

import com.ernest.genesys.common.util.GenesysEntityModelMethod;
import com.ernest.genesys.entity.registration.ApplicantPersonalInformation;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.common.utils.GenUUID;
import com.stately.modules.jpa2.EntityModel2;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author IconU1
 */
@Named(value = "registrationController")
@RequestScoped
public class RegistrationController implements GenesysEntityModelMethod{

   private ApplicantPersonalInformation applicant = new ApplicantPersonalInformation();
   @Inject private CrudService crudService;
   private UploadedFile file;
   private boolean renderDetailLogin = false;
   private boolean renderSearchPanel = true;
    public RegistrationController() {
    }
    
    

   

    @Override
    public void saveMethod() {
        applicant.setApplicantCode(GenUUID.getRandomUUID());
        if(crudService.saveEntity(applicant) !=null)
        {
            Msg.successSave();
            applicant = new ApplicantPersonalInformation();
        }
        else{
            Msg.failedSave();
        }
    }
    
     public void upload() {
        if(file != null) {
           
        }
    }
     
     
     public void showDetailPanel()
     {
         renderDetailLogin = true;
         renderSearchPanel = false;
     }

    @Override
    public void clearMethod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectMethod(EntityModel2 entityModel2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteMethod(EntityModel2 entityModel2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ApplicantPersonalInformation getApplicant() {
        return applicant;
    }

    public void setApplicant(ApplicantPersonalInformation applicant) {
        this.applicant = applicant;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public boolean isRenderDetailLogin() {
        return renderDetailLogin;
    }

    public void setRenderDetailLogin(boolean renderDetailLogin) {
        this.renderDetailLogin = renderDetailLogin;
    }

    public boolean isRenderSearchPanel() {
        return renderSearchPanel;
    }

    public void setRenderSearchPanel(boolean renderSearchPanel) {
        this.renderSearchPanel = renderSearchPanel;
    }
    
    
    
}
