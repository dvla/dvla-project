package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.ServiceTypes;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "serviceTypesController")
@SessionScoped
public class ServiceTypeController implements Dvlamethods, Serializable {

    @Inject
    private CrudService crude;
    private ServiceTypes serviceTypes = new ServiceTypes();
    private List<ServiceTypes> serviceTypessList = new ArrayList<>();

    public ServiceTypeController() {
    }

    @Override
    public void saveMethod() {

        serviceTypes.setId(serviceTypes.getServiceTypeCode());

        if (crude.save(serviceTypes) != null) {
            Msg.successSave();
            serviceTypes = new ServiceTypes();
        } else {
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clearMethod() {
        serviceTypes = new ServiceTypes();
        JsfUtil.resetViewRoot();
    }

    public void selectMethod(ServiceTypes center) {
        serviceTypes = center;
    }

    public void deleteMethod(ServiceTypes center) {
        if (crude.delete(center, false)) {
            Msg.successDelete();
        } else {
            Msg.failedDelete();
        }
        //serviceTypes = center;
    }

    public ServiceTypes getServiceTypes() {
        return serviceTypes;
    }

    public void setServiceTypes(ServiceTypes serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    public List<ServiceTypes> getServiceTypessList() {
        serviceTypessList = crude.findAll(ServiceTypes.class);
        return serviceTypessList;
    }

    public void setServiceTypessList(List<ServiceTypes> serviceTypessList) {
        this.serviceTypessList = serviceTypessList;
    }

}
