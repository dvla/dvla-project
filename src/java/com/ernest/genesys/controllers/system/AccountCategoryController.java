package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.AccountCategory;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "accountCategoryController")
@SessionScoped
public class AccountCategoryController implements Dvlamethods, Serializable {

    @Inject
    private CrudService crude;
    private AccountCategory accountCategory = new AccountCategory();
    private List<AccountCategory> accountCategorysList = new ArrayList<>();

    public AccountCategoryController() {
    }

    @Override
    public void saveMethod() {
//        idServices.accountCategoryId(accountCategory);
        accountCategory.setCategoryCode(GenUUID.getRandomUUID());
        if (crude.save(accountCategory) != null) {
            Msg.successSave();
            accountCategory = new AccountCategory();
        } else {
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
    }

    @Override
    public void clearMethod() {
        accountCategory = new AccountCategory();
        JsfUtil.resetViewRoot();
    }

    public void selectMethod(AccountCategory center) {
        accountCategory = center;
    }

    public void deleteMethod(AccountCategory center) {
        if (crude.delete(center, false)) {
            Msg.successDelete();
        } else {
            Msg.failedDelete();
        }
        //accountCategory = center;
    }

    public AccountCategory getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(AccountCategory accountCategory) {
        this.accountCategory = accountCategory;
    }

    public List<AccountCategory> getAccountCategorysList() {
        accountCategorysList = crude.findAll(AccountCategory.class);
        return accountCategorysList;
    }

    public void setAccountCategorysList(List<AccountCategory> accountCategorysList) {
        this.accountCategorysList = accountCategorysList;
    }

}
