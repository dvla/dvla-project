/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.VehicleMake;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "vehicleMakeController")
@SessionScoped
public class VehicleMakeController implements Dvlamethods, Serializable {

    @Inject
    private CrudService crude;
    private VehicleMake vehicleMake = new VehicleMake();
    private List<VehicleMake> vehicleMakesList = new ArrayList<>();

    public VehicleMakeController() {
    }

    @Override
    public void saveMethod() {
        vehicleMake.setId(vehicleMake.getVehicleMakeCode());

        if (crude.save(vehicleMake) != null) {
            Msg.successSave();
            vehicleMake = new VehicleMake();
        } else {
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clearMethod() {
        vehicleMake = new VehicleMake();
        JsfUtil.resetViewRoot();
    }

    public void selectMethod(VehicleMake center) {
        vehicleMake = center;
    }

    public void deleteMethod(VehicleMake center) {
        if (crude.delete(center, false)) {
            Msg.successDelete();
        } else {
            Msg.failedDelete();
        }
        //vehicleMake = center;
    }

    public VehicleMake getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(VehicleMake vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public List<VehicleMake> getVehicleMakesList() {
        vehicleMakesList = crude.findAll(VehicleMake.class);
        return vehicleMakesList;
    }

    public void setVehicleMakesList(List<VehicleMake> vehicleMakesList) {
        this.vehicleMakesList = vehicleMakesList;
    }

}
