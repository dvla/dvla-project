/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.Rates;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "rateController")
@SessionScoped
public class RateController implements Serializable, Dvlamethods {

    @Inject
    private CrudService crude;
    private Rates rate = new Rates();
    private List<Rates> ratesList = new ArrayList<>();

    public RateController() {
    }

    @Override
    public void saveMethod() {
        
        rate.setId(GenUUID.getRandomUUID());

        if (crude.save(rate) != null) {
            Msg.successSave();
            rate = new Rates();
        } else {
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
    }

    public void selectMethod(Rates items) {
        rate = items;
    }

    @Override
    public void clearMethod() {
        rate = new Rates();
        ratesList = new ArrayList<>(crude.findAll(Rates.class));
    }

    public Rates getRates() {
        return rate;
    }

    public void setRates(Rates rate) {
        this.rate = rate;
    }

    public List<Rates> getRatessList() {
        ratesList = crude.findAll(Rates.class);
        return ratesList;
    }

    public void setRatessList(List<Rates> ratesList) {
        this.ratesList = ratesList;
    }

}
