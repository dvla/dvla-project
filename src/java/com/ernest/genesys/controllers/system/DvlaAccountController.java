/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.system;


import com.ernest.genesys.entity.system_old.DvlaAccount;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "dvlaAccountController")
@SessionScoped
public class DvlaAccountController  implements Dvlamethods,Serializable {

   @Inject private CrudService crude ;
   private DvlaAccount dvlaAccount = new DvlaAccount();
   private List<DvlaAccount> dvlaAccountsList = new ArrayList<>();
   
   
  
    public DvlaAccountController() {
    }
    
    

    @Override
    public void saveMethod() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>saving");
        dvlaAccount.setAccountCode(GenUUID.getRandomUUID());
        System.out.println("THE ID >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+dvlaAccount.getId());
        if(crude.save(dvlaAccount) != null)
        {
            Msg.successSave();
            dvlaAccount = new DvlaAccount();
        }
        else{
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clearMethod() {
        dvlaAccount = new DvlaAccount();
        JsfUtil.resetViewRoot();
    }
    
    public void selectMethod(DvlaAccount center)
    {
        dvlaAccount = center;
    }
    public void deleteMethod(DvlaAccount center)
    {
        if(crude.delete(center, false))
        {
            Msg.successDelete();
        }
        else{
            Msg.failedDelete();
        }
        //dvlaAccount = center;
    }

    public DvlaAccount getDvlaAccount() {
        return dvlaAccount;
    }

    public void setDvlaAccount(DvlaAccount dvlaAccount) {
        this.dvlaAccount = dvlaAccount;
    }

    public List<DvlaAccount> getDvlaAccountsList() {
        dvlaAccountsList = crude.findAll(DvlaAccount.class);
        return dvlaAccountsList;
    }

    public void setDvlaAccountsList(List<DvlaAccount> dvlaAccountsList) {
        this.dvlaAccountsList = dvlaAccountsList;
    }
    
    
    
    
}
