/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.RateItems;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author IconU1
 */
@Named(value = "rateItemsController")
@SessionScoped
public class RateItemsController implements Serializable, Dvlamethods {

    private CrudService crude;
    private RateItems rateItems = new RateItems();
    private List<RateItems> rateItemssList = new ArrayList<>();

    public RateItemsController() {
    }

    @Override
    public void saveMethod() {
        rateItems.setId(GenUUID.getRandomUUID());
        if (crude.save(rateItems) != null) {
            Msg.successSave();
            rateItems = new RateItems();
        } else {
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void selectMethod(RateItems items)
    {
        rateItems = items;
    }

    @Override
    public void clearMethod() {
        rateItems = new RateItems();
        rateItemssList = new ArrayList<>(crude.findAll(RateItems.class));
    }

    public RateItems getRateItems() {
        return rateItems;
    }

    public void setRateItems(RateItems rateItems) {
        this.rateItems = rateItems;
    }

    public List<RateItems> getRateItemssList() {
        rateItemssList = crude.findAll(RateItems.class);
        return rateItemssList;
    }

    public void setRateItemssList(List<RateItems> rateItemssList) {
        this.rateItemssList = rateItemssList;
    }
    
    
    

}
