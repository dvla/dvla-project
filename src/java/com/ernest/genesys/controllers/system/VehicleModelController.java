/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.VehicleModel;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "vehicleModelController")
@SessionScoped
public class VehicleModelController implements Dvlamethods, Serializable {

    @Inject
    private CrudService crude;
    private VehicleModel vehicleModel = new VehicleModel();
    private List<VehicleModel> vehicleModelsList = new ArrayList<>();

    public VehicleModelController() {
    }

    @Override
    public void saveMethod() {
        vehicleModel.setId(vehicleModel.getVehicleModelCode());
        if (crude.save(vehicleModel) != null) {
            Msg.successSave();
            vehicleModel = new VehicleModel();
        } else {
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
    }

    @Override
    public void clearMethod() {
        vehicleModel = new VehicleModel();
        JsfUtil.resetViewRoot();
    }

    public void selectMethod(VehicleModel center) {
        vehicleModel = center;
    }

    public void deleteMethod(VehicleModel center) {
        if (crude.delete(center, false)) {
            Msg.successDelete();
        } else {
            Msg.failedDelete();
        }
        //vehicleModel = center;
    }

    public VehicleModel getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(VehicleModel vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public List<VehicleModel> getVehicleModelsList() {
        vehicleModelsList = crude.findAll(VehicleModel.class);
        return vehicleModelsList;
    }

    public void setVehicleModelsList(List<VehicleModel> vehicleModelsList) {
        this.vehicleModelsList = vehicleModelsList;
    }

}
