/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.LicenseCategory;
import com.ernest.genesys.entity.system_old.LicenseCategory;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "licenseCategoryController")
@SessionScoped
public class LicenseCategoryController implements Serializable {

    private LicenseCategory licenseCategory = new LicenseCategory();
    private List<LicenseCategory> licenseCategorysList = new ArrayList<>();
    @Inject
    private CrudService crudService;

    public LicenseCategoryController() {
    }

    public void saveCenter() {
        licenseCategory.setId(licenseCategory.getLicense());

        if (crudService.save(licenseCategory) != null) {
            licenseCategorysList = new ArrayList<>();
            licenseCategorysList = crudService.findAll(LicenseCategory.class);
            Msg.successSave();
        } else {
            Msg.failedSave();
        }

    }

    public void clear() {
        licenseCategory = new LicenseCategory();
        licenseCategorysList = crudService.findAll(LicenseCategory.class);
    }

    public void selectCenter(LicenseCategory center) {
        licenseCategory = center;
    }

    public void deleteCenter(LicenseCategory center) {
        if (crudService.delete(center, false)) {
            Msg.successDelete();
            licenseCategorysList.remove(center);
        } else {
            Msg.failedDelete();
        }
    }

    public LicenseCategory getLicenseCategory() {
        return licenseCategory;
    }

    public void setLicenseCategory(LicenseCategory licenseCategory) {
        this.licenseCategory = licenseCategory;
    }

    public List<LicenseCategory> getLicenseCategorysList() {
        licenseCategorysList = crudService.findAll(LicenseCategory.class);
        return licenseCategorysList;
    }

    public void setLicenseCategorysList(List<LicenseCategory> licenseCategorysList) {
        this.licenseCategorysList = licenseCategorysList;
    }

}
