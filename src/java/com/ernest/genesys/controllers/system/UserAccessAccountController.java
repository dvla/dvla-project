package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system.UserAccessAccount;
import com.ernest.genesys.services.util.CrudService;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 */
@Named(value = "userAccessAccountController")
@SessionScoped
public class UserAccessAccountController implements Serializable {

    @Inject
    CrudService crudService;
    private UserAccessAccount accessAccount = new UserAccessAccount();

    public UserAccessAccountController() {
    }

    public void saveButton() {

    }

    public void selectButton(UserAccessAccount account) {
        accessAccount = account;
    }

    public void updateButton() {
    }

    public UserAccessAccount getAccessAccount() {
        return accessAccount;
    }

    public void setAccessAccount(UserAccessAccount accessAccount) {
        this.accessAccount = accessAccount;
    }

}
