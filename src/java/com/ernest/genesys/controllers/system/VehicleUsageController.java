package com.ernest.genesys.controllers.system;


import com.ernest.genesys.entity.system_old.VehicleUsage;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "vehicleUsageController")
@SessionScoped
public class VehicleUsageController  implements Dvlamethods,Serializable {

   @Inject private CrudService crude ;
   private VehicleUsage vehicleUsage = new VehicleUsage();
   private List<VehicleUsage> vehicleUsagesList = new ArrayList<>();
   
   
  
    public VehicleUsageController() {
    }
    
    

    @Override
    public void saveMethod() {
        vehicleUsage.setId(vehicleUsage.getVehicleUsageCode());
        if(crude.save(vehicleUsage) != null)
        {
            Msg.successSave();
            vehicleUsage = new VehicleUsage();
        }
        else{
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
    }

    @Override
    public void clearMethod() {
        vehicleUsage = new VehicleUsage();
        JsfUtil.resetViewRoot();
    }
    
    public void selectMethod(VehicleUsage center)
    {
        vehicleUsage = center;
    }
    public void deleteMethod(VehicleUsage center)
    {
        if(crude.delete(center, false))
        {
            Msg.successDelete();
        }
        else{
            Msg.failedDelete();
        }
        //vehicleUsage = center;
    }

    public VehicleUsage getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(VehicleUsage vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    public List<VehicleUsage> getVehicleUsagesList() {
        vehicleUsagesList = crude.findAll(VehicleUsage.class);
        return vehicleUsagesList;
    }

    public void setVehicleUsagesList(List<VehicleUsage> vehicleUsagesList) {
        this.vehicleUsagesList = vehicleUsagesList;
    }
    
    
    
    
}
