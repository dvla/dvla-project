package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system_old.VehicleType;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "vehicleTypeController")
@SessionScoped
public class VehicleTypeController implements Dvlamethods, Serializable {

    @Inject
    private CrudService crude;
    private VehicleType vehicleType = new VehicleType();
    private List<VehicleType> vehicleTypesList = new ArrayList<>();

    public VehicleTypeController() {
    }

    @Override
    public void saveMethod() {
        vehicleType.setId(vehicleType.getVehicleTypeCode());
        if (crude.save(vehicleType) != null) {
            Msg.successSave();
            vehicleType = new VehicleType();
        } else {
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
    }

    @Override
    public void clearMethod() {
        vehicleType = new VehicleType();
        JsfUtil.resetViewRoot();
    }

    public void selectMethod(VehicleType center) {
        vehicleType = center;
    }

    public void deleteMethod(VehicleType center) {
        if (crude.delete(center, false)) {
            Msg.successDelete();
        } else {
            Msg.failedDelete();
        }
        //vehicleType = center;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public List<VehicleType> getVehicleTypesList() {
        vehicleTypesList = crude.findAll(VehicleType.class);
        return vehicleTypesList;
    }

    public void setVehicleTypesList(List<VehicleType> vehicleTypesList) {
        this.vehicleTypesList = vehicleTypesList;
    }

}
