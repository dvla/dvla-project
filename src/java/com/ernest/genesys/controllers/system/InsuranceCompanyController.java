package com.ernest.genesys.controllers.system;


import com.ernest.genesys.entity.system_old.InsuranceCompany;
import com.ernest.genesys.services.common.CommonServices;
import com.ernest.genesys.services.common.Dvlamethods;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.JsfUtil;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Ernest
 */
@Named(value = "insuranceCompanyController")
@SessionScoped
public class InsuranceCompanyController  implements Dvlamethods,Serializable {

   @Inject private CommonServices commonService ;
   @Inject private CrudService crude ;
   private InsuranceCompany insuranceCompany = new InsuranceCompany();
   private List<InsuranceCompany> insuranceCompanysList = new ArrayList<>();
   private String searchValue = null;
   private String searchAttribute = InsuranceCompany._insurnaceCompanyName;
   
   
  
    public InsuranceCompanyController() {
    }
    
    

    @Override
    public void saveMethod() {
        insuranceCompany.setId(GenUUID.getRandomUUID());
        if(crude.save(insuranceCompany) != null)
        {
            Msg.successSave();
            insuranceCompany = new InsuranceCompany();
        }
        else{
            Msg.failedSave();
        }
    }

    @Override
    public void searchMethod() {
        insuranceCompanysList = new ArrayList<>(commonService.searchInsuranceCompany(searchValue, searchAttribute));
    }

    @Override
    public void clearMethod() {
        insuranceCompany = new InsuranceCompany();
        JsfUtil.resetViewRoot();
    }
    
    public void selectMethod(InsuranceCompany center)
    {
        insuranceCompany = center;
    }
    public void deleteMethod(InsuranceCompany center)
    {
        if(crude.delete(center, false))
        {
            Msg.successDelete();
        }
        else{
            Msg.failedDelete();
        }
        //insuranceCompany = center;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearchAttribute() {
        return searchAttribute;
    }

    public void setSearchAttribute(String searchAttribute) {
        this.searchAttribute = searchAttribute;
    }

    public List<InsuranceCompany> getInsuranceCompanysList() {
        //insuranceCompanysList = crude.findAll(InsuranceCompany.class);
        return insuranceCompanysList;
    }

    public void setInsuranceCompanysList(List<InsuranceCompany> insuranceCompanysList) {
        this.insuranceCompanysList = insuranceCompanysList;
    }
    
    
    
    
}
