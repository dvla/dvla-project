package com.ernest.genesys.controllers.common;

import com.ernest.genesys.entity.driver_services.DriverServices;
import com.ernest.genesys.entity.system.Activity;
import com.ernest.genesys.entity.system.DvlaCenters;
import com.ernest.genesys.entity.system_old.AccountCategory;
import com.ernest.genesys.entity.system_old.DvlaAccount;
import com.ernest.genesys.entity.system_old.RateItems;
import com.ernest.genesys.entity.system_old.ServiceType;
import com.ernest.genesys.enums.ActivityType;
import com.ernest.genesys.enums.LicenseClass;
import com.ernest.genesys.enums.RateType;
import com.ernest.genesys.enums.ServiceCategory;
import com.ernest.genesys.services.common.CommonServices;
import com.icsecurities.common.entities.Country;
import com.stately.common.constants.Gender;
import com.stately.common.constants.IdType;
import com.stately.common.constants.Region;
import com.stately.common.constants.Relationship;
import com.stately.common.constants.Title;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "commonOption")
@SessionScoped
public class CommonOption implements Serializable {

    @Inject
    private CommonServices commonServices;

    public CommonOption() {
    }

    public SelectItem[] passOrFailedList(String headerTitle) {
        SelectItem[] dataSelectItems = null;
        try {
            dataSelectItems = new SelectItem[3];
            dataSelectItems[0] = new SelectItem(null, headerTitle);
            dataSelectItems[1] = new SelectItem("Passed", "Passed");
            dataSelectItems[2] = new SelectItem("Failed", "Failed");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataSelectItems;
    }

    public List<ActivityType> getActivityTypeList() {
        return Arrays.asList(ActivityType.values());
    }

    public List<DvlaCenters> getDvlaCenterList() {
        return commonServices.getAllDvlaCenter();
    }

    public List<Gender> getGendersList() {
        return Gender.humanGender();
    }

    public List<Title> getTitleList() {
        return Arrays.asList(Title.values());
    }

    public List<ServiceType> getAllServiceTypes() {
        return Arrays.asList(ServiceType.values());
    }

    public List<Relationship> getRelationshipList() {
        return Arrays.asList(Relationship.values());
    }

    public List<RateType> getRateTypeList() {
        return Arrays.asList(RateType.values());
    }

    public List<ServiceCategory> serviceCategoryList() {
        return Arrays.asList(ServiceCategory.values());
    }

    public List<Region> getRegionList() {
        return Arrays.asList(Region.values());
    }

    public List<LicenseClass> getLicenseClassList() {
        return Arrays.asList(LicenseClass.values());
    }

    public List<IdType> getIdTypeList() {
        return Arrays.asList(IdType.values());
    }

    public List<AccountCategory> accountCategoryList() {
        return commonServices.getAllAccountCategory();
    }

    public List<DvlaAccount> dvlaAccountList() {
        return commonServices.getAllDvlaAccount();
    }

    public List<RateItems> rateItemsList() {
        return commonServices.getAllRateItems();
    }

    public List<Country> getCountryList() {
        return commonServices.getCountries();
    }

    public List<DriverServices> getDriverServices() {
        return commonServices.getAllDriverServices();
    }

    public List<Activity> getActiveActivities() {
        return commonServices.findAll(Activity.class, false);
    }
}
