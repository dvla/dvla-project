package com.ernest.genesys.controllers.applicants;

import com.ernest.genesys.common.util.PageCommonInputs;
import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.enums.ServiceStatus;
import com.ernest.genesys.services.invoicing.DriverInvoiceServices;
import com.ernest.genesys.services.util.CrudService;
import com.stately.modules.web.jsf.Msg;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 06 June 2016
 */
@Named(value = "applicantController")
@ViewScoped
public class ApplicantController implements Serializable {

    @Inject
    CrudService crudService;
    @Inject
    DriverInvoiceServices driverInvoiceServices;

    private Applicant applicant = null;
    private PageCommonInputs pageCommonInputs = new PageCommonInputs();
    private List<Applicant> listOfApplicants = new ArrayList<>();
    private String invoiceNumber;

    public ApplicantController() {
    }

    @PostConstruct
    public void init() {
        clearMethod();
    }

    public void saveMethod() {
        try {

            if (crudService.save(applicant) != null) {
                Msg.genericInfo(applicant.getApplicantName() + " Saved Successfully");
                clearMethod();
            } else {
                Msg.genericError("Failed Saving " + applicant.getApplicantName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchMethod() {
        try {
            if (invoiceNumber.isEmpty() || invoiceNumber == null) {
                Msg.genericError("Please Enter Invoice Number");
                return;
            }

            Transactions transactions = driverInvoiceServices.searchInvoiceNumber(invoiceNumber, ServiceStatus.PENDING);
            if (null != transactions) {
                applicant = transactions.getApplicantId();
            } else if (transactions == null) {
                Msg.genericError("No Applicant Found ");
            }
            transactions = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void viewAllMethod() {
        try {
            listOfApplicants = crudService.findAll(Applicant.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectMethod(Applicant a) {
        applicant = a;
    }

    public void deleteMethod(Applicant a) {
        try {

            if (crudService.delete(a, false)) {
                Msg.genericInfo(applicant.getApplicantName() + " Deleted Successfully");
                viewAllMethod();
            } else {
                Msg.genericError("Failed Deleting " + applicant.getApplicantName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearMethod() {
        applicant = null;
        viewAllMethod();
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS AND SETTERS">
    public Applicant getApplicant() {
        return applicant;
    }

    public List<Applicant> getListOfApplicants() {
        return listOfApplicants;
    }

    public void setListOfApplicants(List<Applicant> listOfApplicants) {
        this.listOfApplicants = listOfApplicants;
    }

    public void setApplicant(Applicant activity) {
        this.applicant = activity;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public PageCommonInputs getPageCommonInputs() {
        return pageCommonInputs;
    }

    public void setPageCommonInputs(PageCommonInputs pageCommonInputs) {
        this.pageCommonInputs = pageCommonInputs;
    }
//</editor-fold>

}
