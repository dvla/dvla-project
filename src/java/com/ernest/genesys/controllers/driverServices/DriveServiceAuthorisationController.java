package com.ernest.genesys.controllers.driverServices;

import com.ernest.genesys.common.util.AppCommonUtils;
import com.ernest.genesys.common.util.DriverServicesProcessTabs;
import com.ernest.genesys.controllers.UserSession;
import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.entity.system.ApplicantServiceActivity;
import com.ernest.genesys.entity.system.ServiceActivity;
import com.ernest.genesys.enums.ServiceStatus;
import com.ernest.genesys.enums.TransactionStatus;
import com.ernest.genesys.services.common.CommonServices;
import com.ernest.genesys.services.invoicing.DriverInvoiceServices;
import com.ernest.genesys.services.util.CrudService;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 03 June 2016
 */
@Named(value = "driveServiceAuthorisationController")
@SessionScoped
public class DriveServiceAuthorisationController implements Serializable {

    @Inject
    CommonServices commonServices;
    @Inject
    CrudService crudService;
    @Inject
    DriverInvoiceServices driverInvoiceServices;
    @Inject
    UserSession userSession;

    private Transactions transactions = new Transactions();
    private Applicant applicant = new Applicant();
    private DriverServicesProcessTabs processTabs = new DriverServicesProcessTabs();
    private String invoiceNumber, displayPanelType = null;
    private List<ApplicantServiceActivity> listOfServiceTest = null;

    public DriveServiceAuthorisationController() {
    }

    @PostConstruct
    public void init() {
        try {
            transactions = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancel() {

        transactions = null;
        invoiceNumber = null;

    }

    public void searchSerivceInvoice() {

        if (invoiceNumber.isEmpty() || invoiceNumber == null) {
            Msg.genericError("Please Enter Invoice Number");
            return;
        }

        transactions = driverInvoiceServices.searchInvoiceNumber(invoiceNumber, ServiceStatus.PROCESSING);

        if (null != transactions) {

            listOfServiceTest = new ArrayList<>();
            listOfServiceTest = driverInvoiceServices.searchApplicantServiceActivities(transactions);

        } else {
            Msg.genericError("No Invoice Number " + invoiceNumber + " Processing Being Processed");
        }

    }

    public void approve() {
        try {

            boolean passed = true;
            for (ApplicantServiceActivity eachTest : listOfServiceTest) {
                if (eachTest.getActivityStatus().equalsIgnoreCase("Failed")) {
                    passed = false;
                }
            }

            if (passed) {
                transactions.setStatus(ServiceStatus.APPROVED);
                if (crudService.update(transactions)) {
                    Msg.genericInfo(transactions.getServiceCode().getServiceName() + " Service Approved - Pending For Card Printing");
                    cancel();
                }
            } else {
                Msg.genericError("Failed Test Cannot Be Approved");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void decline() {
        try {

            transactions.setStatus(ServiceStatus.DECLINED);
            transactions.setTransactionStatus(TransactionStatus.ARCHIVED);
//            for SMS , state the reasons for declining 

            if (crudService.update(transactions)) {
                Msg.genericInfo("Service Application Declined");
                cancel();
            } else {
                Msg.genericError("Failed Decling Service Application");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS AND SETTERS">
    public Transactions getTransactions() {
        return transactions;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public List<ApplicantServiceActivity> getListOfServiceTest() {
        return listOfServiceTest;
    }

    public void setListOfServiceTest(List<ApplicantServiceActivity> listOfServiceTest) {
        this.listOfServiceTest = listOfServiceTest;
    }

    public String getDisplayPanelType() {
        return displayPanelType;
    }

    public void setDisplayPanelType(String displayPanelType) {
        this.displayPanelType = displayPanelType;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }

    public DriverServicesProcessTabs getProcessTabs() {
        return processTabs;
    }

    public void setProcessTabs(DriverServicesProcessTabs processTabs) {
        this.processTabs = processTabs;
    }
//</editor-fold>

}
