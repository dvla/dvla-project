package com.ernest.genesys.controllers.driverServices;

import com.ernest.genesys.common.util.AppCommonUtils;
import com.ernest.genesys.common.util.DriverServicesProcessTabs;
import com.ernest.genesys.controllers.UserSession;
import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.entity.system.ApplicantServiceActivity;
import com.ernest.genesys.entity.system.ServiceActivity;
import com.ernest.genesys.enums.ServiceStatus;
import com.ernest.genesys.services.common.CommonServices;
import com.ernest.genesys.services.invoicing.DriverInvoiceServices;
import com.ernest.genesys.services.util.CrudService;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 03 June 2016
 */
@Named(value = "driverInvoiceProcessingController")
@SessionScoped
public class DriverInvoiceProcessingController implements Serializable {

    @Inject
    CommonServices commonServices;
    @Inject
    DriverInvoiceServices driverInvoiceServices;
    @Inject
    UserSession userSession;

    private Transactions transactions = new Transactions();
    private Applicant applicant = new Applicant();
    private DriverServicesProcessTabs processTabs = new DriverServicesProcessTabs();
    private String currentStep = "1";
    private String invoiceNumber, displayPanelType = null;
    private List<ServiceActivity> listOfServiceTest = null;

    public DriverInvoiceProcessingController() {
    }

    @PostConstruct
    public void init() {
        try {
            transactions = null;
            processTabs.setAppliedService("active");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void currentTab() {
        processTabs = AppCommonUtils.getCurrentProcessStep(currentStep);
    }

    public void cancel() {

        transactions = null;
        invoiceNumber = null;

    }

    public void searchSerivceInvoice() {

        if (invoiceNumber.isEmpty() || invoiceNumber == null) {
            Msg.genericError("Please Enter Invoice Number");
            return;
        }

        processTabs.setAppliedService("active");
        transactions = driverInvoiceServices.searchInvoiceNumber(invoiceNumber, ServiceStatus.PENDING);

        if (null != transactions) {

            if (!transactions.getApplicantId().isBiometricData()) {
                Msg.genericError("No Biometric Details Caputured  Applicant Of " + transactions.getApplicantId().getApplicantName());
            }
            
            listOfServiceTest = new ArrayList<>();
            listOfServiceTest = driverInvoiceServices.getServiceTests(transactions);

        } else {
            Msg.genericError("No Service Applied For Invoice " + invoiceNumber);
        }
    }

    public void postBioData() {

        int counter = 0;
        for (ServiceActivity eachTest : listOfServiceTest) {

            ApplicantServiceActivity applicantTestResults = new ApplicantServiceActivity();
            applicantTestResults.setApplicant(transactions.getApplicantId());
            applicantTestResults.setActivityStatus(eachTest.getTestResults());
            applicantTestResults.setServiceActivity(eachTest);
            applicantTestResults.setTransactions(transactions);
            applicantTestResults.setCreatedDate(new Date());
            applicantTestResults.setLastModifiedBy(userSession.getFullName());
            
            if (commonServices.save(applicantTestResults) != null) {
                counter++;
            }

        }

        boolean allTestSaved = counter == listOfServiceTest.size();

        transactions.setStatus(ServiceStatus.PROCESSING);
        Transactions trxn = commonServices.save(transactions);

        if (allTestSaved && trxn != null) {
            Msg.genericInfo(transactions.getServiceCode().getServiceName() + " Service Of " + transactions.getApplicantId().getApplicantName() + " Posted Successfully ");
            cancel();
        } else {
            Msg.genericError("Failed Posting Applied Service ");
        }

    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS AND SETTERS">
    public String getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public List<ServiceActivity> getListOfServiceTest() {
        return listOfServiceTest;
    }

    public void setListOfServiceTest(List<ServiceActivity> listOfServiceTest) {
        this.listOfServiceTest = listOfServiceTest;
    }

    public String getDisplayPanelType() {
        return displayPanelType;
    }

    public void setDisplayPanelType(String displayPanelType) {
        this.displayPanelType = displayPanelType;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }

    public DriverServicesProcessTabs getProcessTabs() {
        return processTabs;
    }

    public void setProcessTabs(DriverServicesProcessTabs processTabs) {
        this.processTabs = processTabs;
    }
//</editor-fold>

}
