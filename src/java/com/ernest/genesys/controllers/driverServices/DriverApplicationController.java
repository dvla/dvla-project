package com.ernest.genesys.controllers.driverServices;

import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.ernest.genesys.enums.ServiceStatus;
import com.ernest.genesys.enums.ServiceType;
import com.ernest.genesys.services.util.CrudService;
import com.stately.common.utils.GenUUID;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 05 June 2016
 */
@Named(value = "driverApplicationController")
@SessionScoped
public class DriverApplicationController implements Serializable {

    private Applicant applicant = new Applicant();
    private Transactions transactions = new Transactions();
    private @Inject
    CrudService crudService;

    public DriverApplicationController() {
    }

    public void saveApplication() {
        try {

            applicant.setId(GenUUID.getRandomUUID());
            transactions.setTransactionId(GenUUID.getRandomUUID());
            transactions.setApplicantId(applicant);
            transactions.setServiceType(ServiceType.DRIVER);
            transactions.setStatus(ServiceStatus.PENDING);
            transactions.setInvoiceNo(GenUUID.getRandomUUID(8));

            if (null != crudService.save(applicant) && null != crudService.save(transactions)) {
                Msg.successSave();
                Msg.genericInfo("Invoice Number " + transactions.getInvoiceNo());
                cancel();
            } else {
                Msg.failedSave();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancel() {
        try {
            applicant = new Applicant();
            transactions = new Transactions();
        } catch (Exception e) {
        }
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }

}
