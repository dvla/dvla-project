package com.ernest.genesys.controllers.settings;

import com.ernest.genesys.common.util.PageCommonInputs;
import com.ernest.genesys.entity.system.Activity;
import com.ernest.genesys.services.common.CommonServices;
import com.stately.modules.web.jsf.Msg;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 06 June 2016
 */
@Named(value = "applicationRequirementController")
@ViewScoped
public class ApplicationRequirementController implements Serializable {

    @Inject
    CommonServices commonService;

    private Activity activity = new Activity();
    private PageCommonInputs pageCommonInputs = new PageCommonInputs();
    private List<Activity> listOfRequirements = new ArrayList<>();

    public ApplicationRequirementController() {
    }

    @PostConstruct
    public void init() {
        clearMethod();
    }

    public void saveMethod() {
        try {

            if (commonService.save(activity) != null) {
                Msg.genericInfo(activity.getActivityName() + " Saved Successfully");
                clearMethod();
            } else {
                Msg.genericError("Failed Saving " + activity.getActivityName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchMethod() {
        try {
            listOfRequirements = commonService.findByFieldName(Activity.class, pageCommonInputs.getSearchParameter(), pageCommonInputs.getSearchValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void viewAllMethod() {
        try {
            listOfRequirements = commonService.findAll(Activity.class, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectMethod(Activity a) {
        activity = a;
    }

    public void deleteMethod(Activity a) {
        try {

            if (commonService.delete(a, false)) {
                Msg.genericInfo(activity.getActivityName() + " Deleted Successfully");
                viewAllMethod();
            } else {
                Msg.genericError("Failed Deleting " + activity.getActivityName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearMethod() {
        activity = new Activity();
        viewAllMethod();
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS AND SETTERS">
    public Activity getActivity() {
        return activity;
    }

    public List<Activity> getListOfRequirements() {
        return listOfRequirements;
    }

    public void setListOfRequirements(List<Activity> listOfRequirements) {
        this.listOfRequirements = listOfRequirements;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public PageCommonInputs getPageCommonInputs() {
        return pageCommonInputs;
    }

    public void setPageCommonInputs(PageCommonInputs pageCommonInputs) {
        this.pageCommonInputs = pageCommonInputs;
    }
//</editor-fold>

}
