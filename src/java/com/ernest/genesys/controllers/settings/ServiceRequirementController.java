package com.ernest.genesys.controllers.settings;

import com.ernest.genesys.common.util.PageCommonInputs;
import com.ernest.genesys.entity.system.ServiceActivity;
import com.ernest.genesys.services.common.CommonServices;
import com.stately.modules.web.jsf.Msg;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 * @author Ainoo Dauda
 * @contact 0245 293945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 06 June 2016
 */
@Named(value = "serviceRequirementController")
@ViewScoped
public class ServiceRequirementController implements Serializable {

    @Inject
    CommonServices commonService;

    private ServiceActivity serviceActivity = new ServiceActivity();
    private PageCommonInputs pageCommonInputs = new PageCommonInputs();
    private List<ServiceActivity> listOfRequirements = new ArrayList<>();

    public ServiceRequirementController() {
    }

    @PostConstruct
    public void init() {
        clearMethod();
    }

    public void saveMethod() {
        try {

            if (commonService.save(serviceActivity) != null) {
                Msg.genericInfo(serviceActivity.getService().getServiceName() + " Saved Successfully");
                clearMethod();
            } else {
                Msg.genericError("Failed Saving " + serviceActivity.getService().getServiceName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchMethod() {
        try {
            listOfRequirements = commonService.findByFieldName(ServiceActivity.class, pageCommonInputs.getSearchParameter(), pageCommonInputs.getSearchValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void viewAllMethod() {
        try {
            listOfRequirements = commonService.findAll(ServiceActivity.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectMethod(ServiceActivity a) {
        serviceActivity = a;
    }

    public void deleteMethod(ServiceActivity a) {
        try {

            if (commonService.delete(a, false)) {
                Msg.genericInfo(serviceActivity.getService().getServiceName() + " Deleted Successfully");
                viewAllMethod();
            } else {
                Msg.genericError("Failed Deleting " + serviceActivity.getService().getServiceName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearMethod() {
        serviceActivity = new ServiceActivity();
        viewAllMethod();
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS AND SETTERS">
    public ServiceActivity getServiceActivity() {
        return serviceActivity;
    }

    public List<ServiceActivity> getListOfRequirements() {
        return listOfRequirements;
    }

    public void setListOfRequirements(List<ServiceActivity> listOfRequirements) {
        this.listOfRequirements = listOfRequirements;
    }

    public void setServiceActivity(ServiceActivity activity) {
        this.serviceActivity = activity;
    }

    public PageCommonInputs getPageCommonInputs() {
        return pageCommonInputs;
    }

    public void setPageCommonInputs(PageCommonInputs pageCommonInputs) {
        this.pageCommonInputs = pageCommonInputs;
    }
//</editor-fold>

}
