//package com.ernest.genesys.devices.fingerprint;
//
//
//
//
//
//
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//
//import javax.swing.JButton;
//import javax.swing.JComboBox;
//import javax.swing.JDialog;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//
//import com.dermalog.imaging.capturing.valuetype.CaptureMode;
//import com.dermalog.imaging.capturing.valuetype.DeviceIdentity;
//
//public class LoadDeviceDialog {
//	
//	public enum DialogResult { OK, CANCEL }; 
//
//	public DeviceIdentity _SelectedDeviceIdentity;
//	public CaptureMode _SelectedCaptureMode;
//	
//	private DialogResult _DialogResult;
//	private JDialog _Dialog;
//	private JButton _OKButton;
//	private JButton _CancelButton;
//	private JLabel _DeviceLabel;
//	private JLabel _CaptureModeLabel;
//	private JComboBox _DeviceComboBox;
//	private JComboBox _CaptureModeComboBox;
//	
//	public LoadDeviceDialog() {
//		_SelectedDeviceIdentity = DeviceIdentity.FG_ZF10;
//		_SelectedCaptureMode = CaptureMode.PREVIEW_IMAGE_AUTO_DETECT;
//		this.createGUI();
//	}
//	
//	private void createGUI() {
//		// main dialog
//		_Dialog = new JDialog();
//		_Dialog.setModal(true);
//		_Dialog.setSize(350, 200);
//		_Dialog.setResizable(false);
//		_Dialog.setTitle("Load Device");
//		
//		// gridbag layout constraints object
//		GridBagConstraints layout;
//		
//		// main panel
//		JPanel mainPanel = new JPanel();
//		mainPanel.setLayout(new GridBagLayout());
//		
//		// device label
//		_DeviceLabel = new JLabel("Device:");
//		layout = DemoApplet.createLayoutOptions(0, 0, 0, 0, 5);
//		mainPanel.add(_DeviceLabel, layout);
//		
//		// device combo box
//		DeviceIdentity[] devices = { DeviceIdentity.FG_LF10, DeviceIdentity.FG_PLS1, DeviceIdentity.FG_ZF2 };
//		_DeviceComboBox = new JComboBox(devices);
//		layout = DemoApplet.createLayoutOptions(1, 0, 1, 0, 5);
//		layout.gridwidth = 2;
//		mainPanel.add(_DeviceComboBox, layout);
//		_DeviceComboBox.addActionListener(new ActionListener() {
//
//			public void actionPerformed(ActionEvent arg0) {
//				DeviceComboBox_Changed();
//			}
//		});
//		
//		// capture mode label
//		_CaptureModeLabel = new JLabel("Capture Mode:");
//		layout = DemoApplet.createLayoutOptions(0, 1, 0, 0, 5);
//		mainPanel.add(_CaptureModeLabel, layout);
//		
//		// capture mode combo box
//		CaptureMode[] modes = { CaptureMode.PREVIEW_IMAGE_AUTO_DETECT, CaptureMode.LIVE_IMAGE };
//		_CaptureModeComboBox = new JComboBox(modes);
//		layout = DemoApplet.createLayoutOptions(1, 1, 1, 0, 5);
//		layout.gridwidth = 2;
//		mainPanel.add(_CaptureModeComboBox, layout);
//				
//		// OK button
//		_OKButton = new JButton("OK");
//		layout = DemoApplet.createLayoutOptions(1, 2, 1, 0, 5);
//		mainPanel.add(_OKButton, layout);
//		_OKButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				OKButton_Click();
//			}
//		});
//		
//		// cancel button
//		_CancelButton = new JButton("Cancel");
//		layout = DemoApplet.createLayoutOptions(2, 2, 1, 0, 5);
//		mainPanel.add(_CancelButton, layout);
//		_CancelButton.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				CancelButton_Click();
//			}
//		});
//		
//		_Dialog.add(mainPanel);
//	}
//	
//	private void DeviceComboBox_Changed() {
//		DeviceIdentity scanner = (DeviceIdentity) _DeviceComboBox.getSelectedItem();
//		CaptureMode[] modes = null;
//		if (scanner == DeviceIdentity.FG_LF10) {
//			modes = new CaptureMode[] { CaptureMode.PREVIEW_IMAGE_AUTO_DETECT, CaptureMode.LIVE_IMAGE };
//			
//		} else if (scanner == DeviceIdentity.FG_PLS1) {
//			modes = new CaptureMode[] { CaptureMode.PLAIN_FINGER, CaptureMode.LIVE_IMAGE };
//			
//		} else if (scanner == DeviceIdentity.FG_ZF2) {
//			modes = new CaptureMode[] { CaptureMode.PREVIEW_IMAGE_AUTO_DETECT, CaptureMode.LIVE_IMAGE };
//		}
//		
//		_CaptureModeComboBox.removeAllItems();
//		for (int i = 0; i < modes.length; i++) {
//			_CaptureModeComboBox.addItem(modes[i]);
//		}
//	}
//	
//	private void OKButton_Click() {
//		_SelectedDeviceIdentity = (DeviceIdentity) _DeviceComboBox.getSelectedItem();
//		_SelectedCaptureMode = (CaptureMode) _CaptureModeComboBox.getSelectedItem();
//		_DialogResult = DialogResult.OK;
//		_Dialog.setVisible(false);
//	}
//	
//	private void CancelButton_Click() {
//		_Dialog.setVisible(false);
//	}
//	
//	public DialogResult showDialog() {
//		_DialogResult = DialogResult.CANCEL;
//		_Dialog.setVisible(true);
//		return _DialogResult;
//	}
//	
//}
