package com.ernest.genesys.entity.driver_services;

import com.icsecurities.common.entities.Country;
import com.stately.common.constants.Gender;
import com.stately.common.constants.Title;
import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author IconU1
 */
@Entity
@Table(name = "applicant_information")
public class Applicant extends UniqueEntityModel2 implements Serializable {

    @Column(name = "title")
    @Enumerated(EnumType.STRING)
    private Title title;

    @Column(name = "surname")
    private String surname;

    @Column(name = "othername")
    private String othername;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "residential")
    private String residential;

    @JoinColumn(name = "nationality",referencedColumnName = "id")
    private Country nationality;

    @Column(name = "picture_url")
    private String pictureURL;
    
    
    @Column(name = "biometric_data")
    private boolean biometricData;

    @Column(name = "signature_url")
    private String signatureURL;

    public Applicant() {
    }

    public String getApplicantName() {
        return surname + " " + firstName + " " + othername;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Title getTitle() {
        return title;
    }

    public boolean isBiometricData() {
        return biometricData;
    }

    public void setBiometricData(boolean biometricData) {
        this.biometricData = biometricData;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getOthername() {
        return othername;
    }

    public void setOthername(String othername) {
        this.othername = othername;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getResidential() {
        return residential;
    }

    public void setResidential(String residential) {
        this.residential = residential;
    }

    public Country getNationality() {
        return nationality;
    }

    public void setNationality(Country nationality) {
        this.nationality = nationality;
    }

 

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getSignatureURL() {
        return signatureURL;
    }

    public void setSignatureURL(String signatureURL) {
        this.signatureURL = signatureURL;
    }

}
