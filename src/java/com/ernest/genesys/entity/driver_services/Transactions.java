package com.ernest.genesys.entity.driver_services;

import com.ernest.genesys.entity.system.DvlaCenters;
import com.ernest.genesys.enums.LicenseClass;
import com.ernest.genesys.enums.ServiceStatus;
import com.ernest.genesys.enums.ServiceType;
import com.ernest.genesys.enums.TransactionStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 03 June 2016
 * @time Jun 3, 2016 6:15:27 AM
 */
@Entity
@Table(name = "transactions")
@NamedQueries({
    @NamedQuery(name = "Transactions.findAll", query = "SELECT t FROM Transactions t"),
@NamedQuery(name = Transactions.FIND_TRANACTIONS_BY_INVOICE_NUMBER, query = "SELECT t FROM Transactions t where t.invoiceNo=:invoiceno")
})
public class Transactions implements Serializable {
    
    public static final String FIND_TRANACTIONS_BY_INVOICE_NUMBER="Transactions.FIND_TRANACTIONS_BY_INVOICE_NUMBER";

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "transaction_id")
    private String transactionId;

    @JoinColumn(name = "applicant_id", referencedColumnName = "id")
    @ManyToOne
    private Applicant applicantId;

    @JoinColumn(name = "service_code", referencedColumnName = "service_code")
    @ManyToOne
    private DriverServices serviceCode;

    @JoinColumn(name = "center_code", referencedColumnName = "center_code")
    @ManyToOne
    private DvlaCenters centerCode;

    public static final String _invoiceNo = "invoiceNo";
    @Column(name = "invoice_no")
    private String invoiceNo;

    @Column(name = "date_applied")
    @Temporal(TemporalType.DATE)
    private Date dateApplied = new Date();

    public static final String _status = "status";
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ServiceStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_status")
    private TransactionStatus transactionStatus;

    @Lob
    @Size(max = 65535)
    @Column(name = "comments")
    private String comments;

    @Column(name = "vehicle_type_code")
    private String vehicleTypeCode;

    @Column(name = "chassis_no")
    private String chassisNo;

    @Column(name = "reg_no")
    private String regNo;

    @Column(name = "phone_no")
    private String phoneNo;

    @Enumerated(EnumType.STRING)
    @Column(name = "license_class")
    private LicenseClass licenseClass;

    @Column(name = "coc_no")
    private String cocNo;
    @Column(name = "invoice_amt")
    private Double invoiceAmt;

    @Column(name = "payment_status")
    private String paymentStatus;

    @Column(name = "date_paid")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datePaid;

    @Column(name = "teller_id")
    private String tellerId;

    @Column(name = "transaction_ref_no")
    private String transactionRefNo;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile_no")
    private String mobileNo;
    @Basic(optional = false)

    @Enumerated(EnumType.STRING)
    @Column(name = "service_type")
    private ServiceType serviceType;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "plate_type")
    private String plateType;

    @Column(name = "assigned")
    private Integer assigned;

    @Column(name = "processed_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date processedDate;

    @Column(name = "admin_tel")
    private String adminTel;

    @Column(name = "current_license_class")
    private String currentLicenseClass;

    @Column(name = "reason")
    private String reason;

    @Column(name = "new_license_class")
    private String newLicenseClass;

    @Column(name = "destination")
    private String destination;

    @Column(name = "foreign_license_no")
    private String foreignLicenseNo;

    @Column(name = "issue_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;

    @Column(name = "expiry_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;

    @Column(name = "country")
    private String country;

    @Lob
    @Size(max = 65535)
    @Column(name = "service_details")
    private String serviceDetails;

    public Transactions() {
    }

    public Transactions(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Date getDateApplied() {
        return dateApplied;
    }

    public void setDateApplied(Date dateApplied) {
        this.dateApplied = dateApplied;
    }

    public ServiceStatus getStatus() {
        return status;
    }

    public void setStatus(ServiceStatus status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getVehicleTypeCode() {
        return vehicleTypeCode;
    }

    public void setVehicleTypeCode(String vehicleTypeCode) {
        this.vehicleTypeCode = vehicleTypeCode;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public LicenseClass getLicenseClass() {
        return licenseClass;
    }

    public void setLicenseClass(LicenseClass licenseClass) {
        this.licenseClass = licenseClass;
    }

    public String getCocNo() {
        return cocNo;
    }

    public void setCocNo(String cocNo) {
        this.cocNo = cocNo;
    }

    public Double getInvoiceAmt() {
        return invoiceAmt;
    }

    public void setInvoiceAmt(Double invoiceAmt) {
        this.invoiceAmt = invoiceAmt;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Date getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    public String getTellerId() {
        return tellerId;
    }

    public void setTellerId(String tellerId) {
        this.tellerId = tellerId;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPlateType() {
        return plateType;
    }

    public void setPlateType(String plateType) {
        this.plateType = plateType;
    }

    public Integer getAssigned() {
        return assigned;
    }

    public void setAssigned(Integer assigned) {
        this.assigned = assigned;
    }

    public Date getProcessedDate() {
        return processedDate;
    }

    public void setProcessedDate(Date processedDate) {
        this.processedDate = processedDate;
    }

    public String getAdminTel() {
        return adminTel;
    }

    public void setAdminTel(String adminTel) {
        this.adminTel = adminTel;
    }

    public String getCurrentLicenseClass() {
        return currentLicenseClass;
    }

    public void setCurrentLicenseClass(String currentLicenseClass) {
        this.currentLicenseClass = currentLicenseClass;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getNewLicenseClass() {
        return newLicenseClass;
    }

    public void setNewLicenseClass(String newLicenseClass) {
        this.newLicenseClass = newLicenseClass;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getForeignLicenseNo() {
        return foreignLicenseNo;
    }

    public void setForeignLicenseNo(String foreignLicenseNo) {
        this.foreignLicenseNo = foreignLicenseNo;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Applicant getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Applicant applicantId) {
        this.applicantId = applicantId;
    }

    public DriverServices getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(DriverServices serviceCode) {
        this.serviceCode = serviceCode;
    }

    public DvlaCenters getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(DvlaCenters centerCode) {
        this.centerCode = centerCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(String serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionId != null ? transactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) {
            return false;
        }
        Transactions other = (Transactions) object;
        if ((this.transactionId == null && other.transactionId != null) || (this.transactionId != null && !this.transactionId.equals(other.transactionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return transactionId;
    }

}
