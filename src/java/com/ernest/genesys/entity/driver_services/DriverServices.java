package com.ernest.genesys.entity.driver_services;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 * @time Jun 4, 2016 6:54:38 PM
 */
@Entity
@Table(name = "driver_services")
@NamedQueries({
    @NamedQuery(name = "DriverServices.findAll", query = "SELECT d FROM DriverServices d")})
public class DriverServices implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "service_code")
    private String serviceCode;

    public static final String _serviceName = "serviceName";
    @Column(name = "service_name")
    private String serviceName;

    public static final String _serviceTitle = "serviceTitle";
    @Column(name = "service_title")
    private String serviceTitle;

    @Column(name = "service_url")
    private String serviceUrl;

    public DriverServices() {
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviceCode != null ? serviceCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DriverServices)) {
            return false;
        }
        DriverServices other = (DriverServices) object;
        if ((this.serviceCode == null && other.serviceCode != null) || (this.serviceCode != null && !this.serviceCode.equals(other.serviceCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return serviceName;
    }

}
