package com.ernest.genesys.entity.system;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 * @time Jun 4, 2016 6:54:38 PM
 */
@Entity
@Table(name = "dvla_centers")
@NamedQueries({
    @NamedQuery(name = "DvlaCenters.findAll", query = "SELECT d FROM DvlaCenters d")})
public class DvlaCenters implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "center_code")
    private String centerCode;

    public static final String _centerName="centerName";
    @Column(name = "center_name")
    private String centerName;

    public static final String _centerLocation="centerLocation";
    @Column(name = "center_location")
    private String centerLocation;

    public DvlaCenters() {
    }

    public DvlaCenters(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getCenterLocation() {
        return centerLocation;
    }

    public void setCenterLocation(String centerLocation) {
        this.centerLocation = centerLocation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (centerCode != null ? centerCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DvlaCenters)) {
            return false;
        }
        DvlaCenters other = (DvlaCenters) object;
        if ((this.centerCode == null && other.centerCode != null) || (this.centerCode != null && !this.centerCode.equals(other.centerCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return centerName;
    }

}
