package com.ernest.genesys.entity.system;

import com.ernest.genesys.entity.driver_services.Applicant;
import com.ernest.genesys.entity.driver_services.Transactions;
import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 * @time Jun 4, 2016 9:49:04 PM
 */
@Entity
@Table(name = "applicant_service_activity")
@NamedQueries({
    @NamedQuery(name = "ApplicantServiceActivity.findAll", query = "SELECT a FROM ApplicantServiceActivity a"),
    @NamedQuery(name = ApplicantServiceActivity.FIND_SERVICE_ACTIVITY_BY_APPLICANT, query = "select a from ApplicantServiceActivity a where a.applicant=:applicant")
})
public class ApplicantServiceActivity extends UniqueEntityModel2 implements Serializable {

    public static final String FIND_SERVICE_ACTIVITY_BY_APPLICANT = "ApplicantServiceActivity.FIND_SERVICE_ACTIVITY_BY_APPLICANT";

    public static final String _transactions = "transactions";
    @JoinColumn(name = "transaction", referencedColumnName = "transaction_id")
    private Transactions transactions;

    public static final String _applicant = "applicant";
    @JoinColumn(name = "applicant", referencedColumnName = "id")
    private Applicant applicant;

    @JoinColumn(name = "service_activity", referencedColumnName = "id")
    private ServiceActivity serviceActivity;

    @Column(name = "activity_status")
    private String activityStatus;

    public ApplicantServiceActivity() {
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS AND SETTERS">
    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public ServiceActivity getServiceActivity() {
        return serviceActivity;
    }

    public void setServiceActivity(ServiceActivity serviceActivity) {
        this.serviceActivity = serviceActivity;
    }
//</editor-fold>

}
