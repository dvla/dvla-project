package com.ernest.genesys.entity.system;

import com.ernest.genesys.enums.ActivityType;
import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 * @time Jun 4, 2016 9:49:04 PM
 */
@Entity
@Table(name = "activity")
@NamedQueries({
    @NamedQuery(name = "Activity.findAll", query = "SELECT a FROM Activity a")})
public class Activity extends UniqueEntityModel2 implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String _activityName = "activityName";
    @Column(name = "activity_name")
    private String activityName;

    @Column(name = "activity_code")
    private String activityCode;

    @Column(name = "activity_type")
    @Enumerated(EnumType.STRING)
    private ActivityType activityType;

    public Activity() {
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    @Override
    public String toString() {
        return activityName;
    }

}
