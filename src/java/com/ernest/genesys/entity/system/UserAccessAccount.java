package com.ernest.genesys.entity.system;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 * @time Jun 4, 2016 9:38:02 PM
 */
@Entity
@Table(name = "user_access_account")
@NamedQueries({
    @NamedQuery(name = "UserAccessAccount.findAll", query = "SELECT u FROM UserAccessAccount u")})
public class UserAccessAccount extends UniqueEntityModel2 implements Serializable {

    public static final long serialVersionUID = 1L;

    public static final String _fullName = "fullName";
    @Column(name = "full_name")
    private String fullName;

    public static final String _username = "username";
    @Column(name = "username")
    private String username;

    public static final String _userRole = "userRole";
    @Column(name = "user_role")
    private String userRole;

    public static final String _password = "password";
    @Column(name = "password")
    private String password;

    public static final String _center = "center";
    @JoinColumn(name = "center", referencedColumnName = "center_code")
    private DvlaCenters center;

    public static final String _status = "status";
    @Column(name = "status")
    private String status;

    @Transient
    private String confirmedPassword;

    public UserAccessAccount() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }

    public DvlaCenters getCenter() {
        return center;
    }

    public void setCenter(DvlaCenters center) {
        this.center = center;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return username;
    }

}
