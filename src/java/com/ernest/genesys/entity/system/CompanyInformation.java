package com.ernest.genesys.entity.system;

import com.ernest.genesys.entity.system_old.UserAccount;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com 
 * @date 04 June 2016
 * @time  Jun 4, 2016 6:54:37 PM
 */
@Entity
@Table(name = "company_information")
@NamedQueries({
    @NamedQuery(name = "CompanyInformation.findAll", query = "SELECT c FROM CompanyInformation c")})
public class CompanyInformation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "company_pin")
    private String companyPin;
    @Size(max = 150)
    @Column(name = "business_name")
    private String businessName;
    @Size(max = 50)
    @Column(name = "tin_no")
    private String tinNo;
    @Size(max = 50)
    @Column(name = "business_reg_no")
    private String businessRegNo;
    @Size(max = 150)
    @Column(name = "business_nature")
    private String businessNature;
    @Size(max = 50)
    @Column(name = "post_box")
    private String postBox;
    @Size(max = 50)
    @Column(name = "suburb")
    private String suburb;
    @Size(max = 50)
    @Column(name = "location")
    private String location;
    @Size(max = 10)
    @Column(name = "phone_no")
    private String phoneNo;
    @Size(max = 10)
    @Column(name = "phone_no_2")
    private String phoneNo2;
    @Size(max = 15)
    @Column(name = "fax_no")
    private String faxNo;
    @Size(max = 200)
    @Column(name = "street_address")
    private String streetAddress;
    @Size(max = 50)
    @Column(name = "region")
    private String region;
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    @Size(max = 150)
    @Column(name = "website")
    private String website;
    @Lob
    @Size(max = 65535)
    @Column(name = "administrators")
    private String administrators;
    @Size(max = 50)
    @Column(name = "city")
    private String city;

    public CompanyInformation() {
    }

    public CompanyInformation(String companyPin) {
        this.companyPin = companyPin;
    }

    public String getCompanyPin() {
        return companyPin;
    }

    public void setCompanyPin(String companyPin) {
        this.companyPin = companyPin;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getBusinessRegNo() {
        return businessRegNo;
    }

    public void setBusinessRegNo(String businessRegNo) {
        this.businessRegNo = businessRegNo;
    }

    public String getBusinessNature() {
        return businessNature;
    }

    public void setBusinessNature(String businessNature) {
        this.businessNature = businessNature;
    }

    public String getPostBox() {
        return postBox;
    }

    public void setPostBox(String postBox) {
        this.postBox = postBox;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPhoneNo2() {
        return phoneNo2;
    }

    public void setPhoneNo2(String phoneNo2) {
        this.phoneNo2 = phoneNo2;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAdministrators() {
        return administrators;
    }

    public void setAdministrators(String administrators) {
        this.administrators = administrators;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (companyPin != null ? companyPin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompanyInformation)) {
            return false;
        }
        CompanyInformation other = (CompanyInformation) object;
        if ((this.companyPin == null && other.companyPin != null) || (this.companyPin != null && !this.companyPin.equals(other.companyPin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ernest.genesys.entity.driver_services.CompanyInformation[ companyPin=" + companyPin + " ]";
    }

}
