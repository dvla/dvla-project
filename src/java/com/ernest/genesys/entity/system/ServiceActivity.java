package com.ernest.genesys.entity.system;

import com.ernest.genesys.entity.driver_services.DriverServices;
import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 * @time Jun 4, 2016 9:49:04 PM
 */
@Entity
@Table(name = "service_activity")
@NamedQueries({
    @NamedQuery(name = "ServiceActivity.findAll", query = "SELECT s FROM ServiceActivity s"),
    @NamedQuery(name = ServiceActivity.FIND_DRIVER_SERVICE_ACTIVITIES, query = "select s from ServiceActivity s where s.service=:service")
})
public class ServiceActivity extends UniqueEntityModel2 implements Serializable {

    public static final String FIND_DRIVER_SERVICE_ACTIVITIES = "ServiceActivity.FIND_DRIVER_SERVICE_ACTIVITIES";

    public static final String _service = "service";
    @JoinColumn(name = "service", referencedColumnName = "service_code")
    private DriverServices service;
    @JoinColumn(name = "activity", referencedColumnName = "id")
    private Activity activity;
    @Column(name = "status")
    private String status;

    @Transient
    private String testResults;

    public ServiceActivity() {
    }

    public DriverServices getService() {
        return service;
    }

    public String getTestResults() {
        return testResults;
    }

    public void setTestResults(String testResults) {
        this.testResults = testResults;
    }

    public void setService(DriverServices service) {
        this.service = service;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
