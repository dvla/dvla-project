/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "vehicle_model")
public class VehicleModel extends UniqueEntityModel2 implements Serializable {
 
@Column(name = "vehicle_model_code")
private String vehicleModelCode;
@Column(name = "vehicle_model_name")
private String vehicleModelName;
@JoinColumn(name = "vehicle_make")
private VehicleMake vehicleMake;

    public String getVehicleModelCode() {
        return vehicleModelCode;
    }

    public void setVehicleModelCode(String vehicleModelCode) {
        this.vehicleModelCode = vehicleModelCode;
    }

    public String getVehicleModelName() {
        return vehicleModelName;
    }

    public void setVehicleModelName(String vehicleModelName) {
        this.vehicleModelName = vehicleModelName;
    }

    public VehicleMake getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(VehicleMake vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

   

    @Override
    public String toString() {
        return vehicleModelName;
    }
    
}
