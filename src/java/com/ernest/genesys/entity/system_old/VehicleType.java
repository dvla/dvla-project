/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "vehicle_type")
public class VehicleType extends UniqueEntityModel2 implements Serializable {
 
@Column(name = "vehicle_type_code")
private String vehicleTypeCode;
@Column(name = "vehicle_type_name")
private String vehicleTypeName;

    public String getVehicleTypeCode() {
        return vehicleTypeCode;
    }

    public void setVehicleTypeCode(String vehicleTypeCode) {
        this.vehicleTypeCode = vehicleTypeCode;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }


   

    @Override
    public String toString() {
        return vehicleTypeName;
    }
    
}
