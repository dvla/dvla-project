package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "license_type")
public class LicenseType extends UniqueEntityModel2 implements Serializable {

    @Column(name = "vehicle_make_code")
    private String vehicleMakeCode;
    @Column(name = "vehicle_make_name")
    private String vehicleMakeName;

    public String getVehicleMakeCode() {
        return vehicleMakeCode;
    }

    public void setVehicleMakeCode(String vehicleMakeCode) {
        this.vehicleMakeCode = vehicleMakeCode;
    }

    public String getVehicleMakeName() {
        return vehicleMakeName;
    }

    public void setVehicleMakeName(String vehicleMakeName) {
        this.vehicleMakeName = vehicleMakeName;
    }

    @Override
    public String toString() {
        return vehicleMakeName;
    }

    public LicenseType() {
    }

}
