/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.ernest.genesys.entity.system_old.ServiceType;
import com.ernest.genesys.enums.RateType;
import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "rates")
public class Rates extends UniqueEntityModel2 implements Serializable{

    @JoinColumn(name = "rate_items")
    private RateItems rateItems ;
    
    @Column(name = "rate")
    private Double rate = 0.0;
    
    @Column(name = "rate_type")
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType ;
    
  

    public Rates() {
    }

    public RateItems getRateItems() {
        return rateItems;
    }

    public void setRateItems(RateItems rateItems) {
        this.rateItems = rateItems;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }
    
    

   
    
    

    
    
}
