package com.ernest.genesys.entity.system_old;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com 
 * @date 04 June 2016
 * @time  Jun 4, 2016 6:54:38 PM
 */
@Entity
@Table(name = "gcnet")
@NamedQueries({
    @NamedQuery(name = "Gcnet.findAll", query = "SELECT g FROM Gcnet g")})
public class Gcnet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "chassis_no")
    private String chassisNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "vmake")
    private String vmake;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "vtype")
    private String vtype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "vmodel")
    private String vmodel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mf_year")
    @Temporal(TemporalType.DATE)
    private Date mfYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "vcapacity")
    private String vcapacity;

    public Gcnet() {
    }

    public Gcnet(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public Gcnet(String chassisNo, String vmake, String vtype, String vmodel, Date mfYear, String vcapacity) {
        this.chassisNo = chassisNo;
        this.vmake = vmake;
        this.vtype = vtype;
        this.vmodel = vmodel;
        this.mfYear = mfYear;
        this.vcapacity = vcapacity;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getVmake() {
        return vmake;
    }

    public void setVmake(String vmake) {
        this.vmake = vmake;
    }

    public String getVtype() {
        return vtype;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    public String getVmodel() {
        return vmodel;
    }

    public void setVmodel(String vmodel) {
        this.vmodel = vmodel;
    }

    public Date getMfYear() {
        return mfYear;
    }

    public void setMfYear(Date mfYear) {
        this.mfYear = mfYear;
    }

    public String getVcapacity() {
        return vcapacity;
    }

    public void setVcapacity(String vcapacity) {
        this.vcapacity = vcapacity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chassisNo != null ? chassisNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gcnet)) {
            return false;
        }
        Gcnet other = (Gcnet) object;
        if ((this.chassisNo == null && other.chassisNo != null) || (this.chassisNo != null && !this.chassisNo.equals(other.chassisNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ernest.genesys.entity.driver_services.Gcnet[ chassisNo=" + chassisNo + " ]";
    }

}
