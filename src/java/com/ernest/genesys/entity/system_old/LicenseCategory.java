/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.EntityModel;
import com.stately.modules.jpa2.EntityModel2;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author IconU1
 */
@Entity
@Table(name = "license_category")
public class LicenseCategory extends EntityModel2{
    
    @Id
    @Column(name = "id")
    private String id;
    
    @Column(name = "license")
    private String license;

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }
    
  
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

   

    @Override
    public String toString() {
        return  license;
    }
    
    
}
