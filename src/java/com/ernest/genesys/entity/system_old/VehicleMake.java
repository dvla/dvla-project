/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "vehicle_make")
public class VehicleMake extends UniqueEntityModel2 implements Serializable {
 
@Column(name = "vehicle_make_code")
private String vehicleMakeCode;
@Column(name = "vehicle_make_name")
private String vehicleMakeName;

    public String getVehicleMakeCode() {
        return vehicleMakeCode;
    }

    public void setVehicleMakeCode(String vehicleMakeCode) {
        this.vehicleMakeCode = vehicleMakeCode;
    }

    public String getVehicleMakeName() {
        return vehicleMakeName;
    }

    public void setVehicleMakeName(String vehicleMakeName) {
        this.vehicleMakeName = vehicleMakeName;
    }

  

   

    @Override
    public String toString() {
        return vehicleMakeName;
    }
    
}
