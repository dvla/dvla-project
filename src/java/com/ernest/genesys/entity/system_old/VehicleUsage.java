/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "vehicle_usage")
public class VehicleUsage extends UniqueEntityModel2 implements Serializable {
 
@Column(name = "vehicle_usage_code")
private String vehicleUsageCode;
@Column(name = "vehicle_usage_name")
private String vehicleUsageName;

    public String getVehicleUsageCode() {
        return vehicleUsageCode;
    }

    public void setVehicleUsageCode(String vehicleUsageCode) {
        this.vehicleUsageCode = vehicleUsageCode;
    }

    public String getVehicleUsageName() {
        return vehicleUsageName;
    }

    public void setVehicleUsageName(String vehicleUsageName) {
        this.vehicleUsageName = vehicleUsageName;
    }

   
  

   

    @Override
    public String toString() {
        return vehicleUsageName;
    }
    
}
