package com.ernest.genesys.entity.system_old;

import com.ernest.genesys.entity.system.CompanyInformation;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 
 * @company Icon
 * @email ainoodauda@gmail.com
 * @date 04 June 2016
 * @time  Jun 4, 2016 6:54:39 PM
 */
@Entity
@Table(name = "useraccount")
@NamedQueries({
    @NamedQuery(name = "Useraccount.findAll", query = "SELECT u FROM Useraccount u")})
public class UserAccount implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "account_no")
    private String accountNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "account_type")
    private String accountType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "status")
    private String status;
    @Column(name = "last_login")
    private Integer lastLogin;
    @Size(max = 50)
    @Column(name = "remember_code")
    private String rememberCode;
    @Column(name = "date_created")
    private Integer dateCreated;
    @Lob
    @Size(max = 65535)
    @Column(name = "social")
    private String social;
    @Size(max = 15)
    @Column(name = "admin_tel")
    private String adminTel;
    @Lob
    @Size(max = 65535)
    @Column(name = "login_history")
    private String loginHistory;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountNo")
    private List<PersonalInformation> personalInformationList;
    @JoinColumn(name = "company_pin", referencedColumnName = "company_pin")
    @ManyToOne
    private CompanyInformation companyPin;

    public UserAccount() {
    }

    public UserAccount(String accountNo) {
        this.accountNo = accountNo;
    }

    public UserAccount(String accountNo, String accountType, String username, String email, String password, String status) {
        this.accountNo = accountNo;
        this.accountType = accountType;
        this.username = username;
        this.email = email;
        this.password = password;
        this.status = status;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Integer lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getRememberCode() {
        return rememberCode;
    }

    public void setRememberCode(String rememberCode) {
        this.rememberCode = rememberCode;
    }

    public Integer getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Integer dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public String getAdminTel() {
        return adminTel;
    }

    public void setAdminTel(String adminTel) {
        this.adminTel = adminTel;
    }

    public String getLoginHistory() {
        return loginHistory;
    }

    public void setLoginHistory(String loginHistory) {
        this.loginHistory = loginHistory;
    }

    public List<PersonalInformation> getPersonalInformationList() {
        return personalInformationList;
    }

    public void setPersonalInformationList(List<PersonalInformation> personalInformationList) {
        this.personalInformationList = personalInformationList;
    }

    public CompanyInformation getCompanyPin() {
        return companyPin;
    }

    public void setCompanyPin(CompanyInformation companyPin) {
        this.companyPin = companyPin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountNo != null ? accountNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccount)) {
            return false;
        }
        UserAccount other = (UserAccount) object;
        if ((this.accountNo == null && other.accountNo != null) || (this.accountNo != null && !this.accountNo.equals(other.accountNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ernest.genesys.entity.driver_services.Useraccount[ accountNo=" + accountNo + " ]";
    }

}
