/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.ernest.genesys.enums.RateCategory;
import com.stately.modules.jpa2.EntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author IconU1
 */
@Entity
@Table(name = "driver_vehicle_rates")
public class DriverVehicleRates extends EntityModel2 implements Serializable{
    
    @Id
    @Column(name = "rate_code")
    private String rateCode;
    
    @Column(name = "item_code")
    private String itemCode;
    
    @Column(name = "service_code")
    private String serviceCode;
    
    @Column(name = "vehicle_type")
    private String vehicleType;
    
    @Column(name = "rate_category")
    private RateCategory rateCategory ;
    
    @Column(name = "rate")
    private double rate ;
    
   

    public DriverVehicleRates() {
    }

    public String getRateCode() {
        return rateCode;
    }

    public void setRateCode(String rateCode) {
        this.rateCode = rateCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public RateCategory getRateCategory() {
        return rateCategory;
    }

    public void setRateCategory(RateCategory rateCategory) {
        this.rateCategory = rateCategory;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

  

   
   
    
    
}
