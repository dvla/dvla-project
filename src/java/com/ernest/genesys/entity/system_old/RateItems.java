/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.ernest.genesys.enums.RateType;
import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "rate_items")
public class RateItems extends UniqueEntityModel2 implements Serializable{

    @Column(name = "item_code")
    private String itemCode;
    
    @Column(name = "item_name")
    private String itemName;
    
    @Column(name = "rate_type")
    @Enumerated(EnumType.STRING)
    private RateType rateType ;
    
    @JoinColumn(name = "dvla_account")
    private DvlaAccount dvlaAccount;

    public RateItems() {
    }
    
    

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public RateType getRateType() {
        return rateType;
    }

    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public DvlaAccount getDvlaAccount() {
        return dvlaAccount;
    }

    public void setDvlaAccount(DvlaAccount dvlaAccount) {
        this.dvlaAccount = dvlaAccount;
    }

    @Override
    public String toString() {
        return  itemName ;
    }
    
    
    

    
    
}
