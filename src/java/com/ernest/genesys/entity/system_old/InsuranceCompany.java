/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "insurance_company")
public class InsuranceCompany extends UniqueEntityModel2 implements Serializable {

    public static final String _insurnaceCompanyName = "insurnaceCompanyName";
    @Column(name = "insurance_company_name")
    private String insurnaceCompanyName;
    @Column(name = "telephone")
    private String telephone;
    @Column(name = "company_address")
    private String companyAddress;
    @Column(name = "company_short_name")
    private String companyShortName;

    public String getInsurnaceCompanyName() {
        return insurnaceCompanyName;
    }

    public void setInsurnaceCompanyName(String insurnaceCompanyName) {
        this.insurnaceCompanyName = insurnaceCompanyName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    @Override
    public String toString() {
        return insurnaceCompanyName;
    }

}
