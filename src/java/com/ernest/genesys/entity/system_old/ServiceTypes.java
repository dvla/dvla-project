package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Ernest
 */
@Entity
@Table(name = "service_type")
public class ServiceTypes extends UniqueEntityModel2 implements Serializable {

    @Column(name = "service_category")
    private String serviceCategory;
    @Column(name = "service_type_code")
    private String serviceTypeCode;
    @Column(name = "service_type_name")
    private String serviceTypeName;
    @Column(name = "service_title")
    private String serviceTitle;

    public String getServiceTypeCode() {
        return serviceTypeCode;
    }

    public void setServiceTypeCode(String serviceTypeCode) {
        this.serviceTypeCode = serviceTypeCode;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    @Override
    public String toString() {
        return serviceTypeName;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

   

}
