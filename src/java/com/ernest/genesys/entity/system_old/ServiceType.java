/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system_old;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author IconU1
 */
public enum ServiceType implements MessageResolvable{

    RENEWAL("Renewal","Renewal");
    
    
    
    String label;
    String code;

    private ServiceType(String label, String code) {
        this.label = label;
        this.code = code;
    }

    
    
    @Override
    public String getCode() {
       return code;
    }

    @Override
    public String getLabel() {
        return label;
    }
    
    
}
