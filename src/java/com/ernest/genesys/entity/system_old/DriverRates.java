package com.ernest.genesys.entity.system_old;

import com.ernest.genesys.entity.driver_services.DriverServices;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com 
 * @date 04 June 2016
 * @time  Jun 4, 2016 6:54:38 PM
 */
@Entity
@Table(name = "driver_rates")
@NamedQueries({
    @NamedQuery(name = "DriverRates.findAll", query = "SELECT d FROM DriverRates d")})
public class DriverRates implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "rate_id")
    private String rateId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rate")
    private BigDecimal rate;
    @JoinColumn(name = "item_code", referencedColumnName = "item_code")
    @ManyToOne(optional = false)
    private DriverRateItems itemCode;
    @JoinColumn(name = "service_code", referencedColumnName = "service_code")
    @ManyToOne(optional = false)
    private DriverServices serviceCode;

    public DriverRates() {
    }

    public DriverRates(String rateId) {
        this.rateId = rateId;
    }

    public DriverRates(String rateId, BigDecimal rate) {
        this.rateId = rateId;
        this.rate = rate;
    }

    public String getRateId() {
        return rateId;
    }

    public void setRateId(String rateId) {
        this.rateId = rateId;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public DriverRateItems getItemCode() {
        return itemCode;
    }

    public void setItemCode(DriverRateItems itemCode) {
        this.itemCode = itemCode;
    }

    public DriverServices getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(DriverServices serviceCode) {
        this.serviceCode = serviceCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rateId != null ? rateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DriverRates)) {
            return false;
        }
        DriverRates other = (DriverRates) object;
        if ((this.rateId == null && other.rateId != null) || (this.rateId != null && !this.rateId.equals(other.rateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ernest.genesys.entity.driver_services.DriverRates[ rateId=" + rateId + " ]";
    }

}
