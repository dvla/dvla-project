package com.ernest.genesys.entity.system_old;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Ainoo Dauda
 * @contact 0245 29 3945 / 0206 47 6899
 * @company Icon
 * @email ainoodauda@gmail.com 
 * @date 04 June 2016
 * @time  Jun 4, 2016 6:54:38 PM
 */
@Entity
@Table(name = "driver_rate_items")
@NamedQueries({
    @NamedQuery(name = "DriverRateItems.findAll", query = "SELECT d FROM DriverRateItems d")})
public class DriverRateItems implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "item_code")
    private String itemCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "item_name")
    private String itemName;
    @Size(max = 50)
    @Column(name = "rate_type")
    private String rateType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "dvla_account_code")
    private String dvlaAccountCode;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemCode")
    private List<DriverRates> driverRatesList;

    public DriverRateItems() {
    }

    public DriverRateItems(String itemCode) {
        this.itemCode = itemCode;
    }

    public DriverRateItems(String itemCode, String itemName, String dvlaAccountCode) {
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.dvlaAccountCode = dvlaAccountCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getDvlaAccountCode() {
        return dvlaAccountCode;
    }

    public void setDvlaAccountCode(String dvlaAccountCode) {
        this.dvlaAccountCode = dvlaAccountCode;
    }

    public List<DriverRates> getDriverRatesList() {
        return driverRatesList;
    }

    public void setDriverRatesList(List<DriverRates> driverRatesList) {
        this.driverRatesList = driverRatesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemCode != null ? itemCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DriverRateItems)) {
            return false;
        }
        DriverRateItems other = (DriverRateItems) object;
        if ((this.itemCode == null && other.itemCode != null) || (this.itemCode != null && !this.itemCode.equals(other.itemCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ernest.genesys.entity.driver_services.DriverRateItems[ itemCode=" + itemCode + " ]";
    }

}
