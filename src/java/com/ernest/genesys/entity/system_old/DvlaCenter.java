package com.ernest.genesys.entity.system_old;

import com.stately.modules.jpa2.EntityModel2;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author IconU1
 */
@Entity
@Table(name = "dvla_center")
public class DvlaCenter extends EntityModel2{
    
    @Id
    @Column(name = "id")
    private String id;
    
    @Column(name = "center_code")
    private String centerCode;
    
    @Column(name = "center_name")
    private String centerName;

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getCenterName() {
        return centerName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    @Override
    public String toString() {
        return centerName ;
    }
    
    
}
